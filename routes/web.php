<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/all_employees', 'HomeController@employees');
Route::post('update_profile', 'HomeController@updateUserProfile');
Route::get('/add_employee', 'HumanResourcesController@displayAddmployeePage');
Route::post('/add_employee', 'HumanResourcesController@create');
Route::post('/register', 'Auth\RegisterController@create');
Route::get('/profile', 'HumanResourcesController@showProfile');
Route::get('/edit/{id}', 'HumanResourcesController@editEmployee');
Route::post('/edit/{id}', 'HumanResourcesController@update');
Route::get('/delete/{id}', 'HumanResourcesController@destroy');
Route::get('/profile/{id}', 'RegisterController@profileSearchById');
Route::get('/job_application', 'HrApplicationController@showHrApp');
//application form routes applicant part
Route::get('/applicant_registration', 'ApplicantRegistrationController@showApplicantRegistration');
Route::post('/applicant_registration', 'ApplicantRegistrationController@createRegistrationOfApplicant');
Route::get('/applicant_login', 'ApplicantLoginController@showApplicantLogin');
Route::post('/login_applicant', 'ApplicantRegistrationController@authenticate');
Route::get('/applicant_profile_details', 'ApplicantProfileController@showApplicationProfile');
Route::post('/applicant_profile_details', 'ApplicantProfileController@createProfileOfApplicant');
Route::get('/education', 'ApplicantEducationController@showEducation');
Route::get('/add_education', 'ApplicantEducationController@showAddEducation');
Route::post('/add_education', 'ApplicantEducationController@createEducationOfApplicant');
Route::get('/experience', 'ApplicantExperienceController@showExperience');
Route::get('/add_work_experience', 'ApplicantExperienceController@showAddExperience');
Route::post('/experience', 'ApplicantExperienceController@createExperienceOfApplicant');
Route::get('/referee', 'ApplicantRefereeController@showReferee');
Route::get('/add_referee', 'ApplicantRefereeController@showAddReferee');
Route::post('/referee', 'ApplicantRefereeController@createRefereeOfApplicant');
Route::get('/logout', 'ApplicantRegistrationController@logout');
Route::get('/summary','ApplicantRegistrationController@showSummary');
Route::get('/hr_applicant_display', 'ApplicantRegistrationController@showApplicantsBlade');
Route::get('/applicants_display', 'ApplicantRegistrationController@allInfo');
Route::get('/edit/{id}', 'ApplicantRegistrationController@delete');
Route::get('/curriculumvitae_display/{id}','ApplicantRegistrationController@showCv');
Route::get('/curriculumvitae/{id}','ApplicantRegistrationController@showCv');
Route::get('/attachment/{name}', 'ApplicantEducationController@showAttachment');
Route::get('/awards/{name}','ApplicantExperienceController@showAttachment');
Route::post('/update/{id}', 'ApplicantProfileController@update');
Route::post('/update_education/{id}', 'ApplicantEducationController@update');
Route::post('/update_experience/{id}', 'ApplicantExperienceController@updateExperienceOfApplicant');
Route::post('/update_referee/{id}', 'ApplicantRefereeController@updateRefereeOfApplicant');
Route::get('/delete_profile/{id}', 'ApplicantProfileController@destroy');
Route::get('/delete_education/{id}', 'ApplicantEducationController@destroy');
Route::get('/delete_experience/{id}', 'ApplicantExperienceController@destroy');
Route::get('/delete_referee/{id}', 'ApplicantRefereeController@destroy');
//end of application routes-*
Route::post('/job_application', 'HrApplicationController@create');
Route::post('/job_applicant_details', 'HrApplicationController@createProfileOfApplicant');
Route::get('/applicants', 'HrApplicationController@findAllApplicants');
Route::get('/all_applicants', 'HrApplicationController@applicants');
Route::get('/applicants_cv/{name}', 'HrApplicationController@download');
Route::get('/applicant_delete/{id}', 'HrApplicationController@delete');
Route::get('/users', 'HomeController@fetchUsers');




