<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantEducation extends Model
{
    public function applicantAccount() {
        return $this->belongsTo(ApplicantRegistration::class,'applicant','id');
    }
}
