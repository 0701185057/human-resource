<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantProfile extends Model
{
    public function profile() {
        return $this->belongsTo(ApplicantRegistration::class,'applicant','id');
    }
}
