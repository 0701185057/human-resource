<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantExperience extends Model
{
    public function experience() {
        return $this->belongsTo(ApplicantRegistration::class,'applicant','id');
    }
}
