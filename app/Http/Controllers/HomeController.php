<?php

namespace App\Http\Controllers;

use App\HumanResources;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function fetchAll()
    {
        return HumanResources::with('user')->get();
    }

    public function fetchUsers()
    {
        return User::with('employees')->get();
    }

    public function employees()
    {
        $employees = $this->fetchAll();
        return Datatables::of($employees)
            ->addColumn('created_by', function ($e) {
                return $e->user->first_name." ".$e->user->last_name;
            })
            ->addColumn('edit', function ($employee) {
                return '<button class="btn btn-xs btn-primary" id="edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->addColumn('delete', function ($employee) {
                return '<button class="btn btn-xs btn-primary" id="delete"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
            ->rawColumns(['edit', 'delete'])
            ->make(true);
    }

    public function updateUserProfile(Request $request)
    {
        $data = Auth::user();
        Log::info($request);
        $data->first_name = $request->first_name;
        $data->middle_name = $request->middle_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->phone_number = $request->phone_number;
        $data->id_number = $request->id_number;
        $data->staff_number = $request->staff_number;
        $data->save();
    }
}
