<?php

namespace App\Http\Controllers;

use App\ApplicantReferee;
use App\ApplicantRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Redirect;
use Response;
use DataTables;

class
ApplicantRefereeController extends Controller
{
    public function showReferee()
    {
        return view('referee');
    }
    public function showAddReferee()
    {
        return view('add_referee');
    }
    public function createRefereeOfApplicant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company' => ['required', 'string'],
            'job_title' => ['required','string'],
            'name' => ['required'],
            'phone' => ['required'],
            'email' => ['required','email'],
            'postal_address' => ['required','string'],
            'postal_code' => ['required','string'],
        ]);
        if ($validator->fails()) {
            return redirect::to('referee')->withErrors($validator);
        }
        $data = new ApplicantReferee();
        $data->company = $request->company;
        $data->job_title = $request->job_title;
        $data->name = $request->name;
        $data->phone= $request->phone;
        $data->email= $request->email;
        $data->postal_address = $request->postal_address;
        $data->applicant = Auth::guard('applicant')->user()->id;
        $data->postal_code = $request->postal_code;
        $data->save();
        return view('applicant_profile_details')->with('d', $data);
    }
    public function findByid($id)
    {
        return ApplicantReferee::find($id);
    }
    public function updateRefereeOfApplicant(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'company' => ['required', 'string'],
            'job_title' => ['required','string'],
            'name' => ['required'],
            'phone' => ['required'],
            'email' => ['required','email'],
            'postal_address' => ['required','string'],
            'postal_code' => ['required','string'],
        ]);
        if ($validator->fails()) {
            return json_encode($validator->errors());
            return redirect::to('summary')->withErrors($validator);
        }
        $data = $this->findById($id);
        $data->company = $request->company;
        $data->job_title = $request->job_title;
        $data->name = $request->name;
        $data->phone= $request->phone;
        $data->email= $request->email;
        $data->postal_address = $request->postal_address;
        $data->applicant = Auth::guard('applicant')->user()->id;
        $data->postal_code = $request->postal_code;
        $data->save();
        return redirect::to('summary')->with('information', $this->getApplicantById($data->applicant));
    }
    public function getApplicantById($id){
        return ApplicantRegistration::with('profile', 'education', 'experience', 'referee')
            ->where('id', $id)->first();
    }
    public function destroy($id)
    {
        ApplicantReferee::where('id', $id)->delete();

        return Redirect::to('summary')->with('success', 'Product deleted successfully');
    }
}

