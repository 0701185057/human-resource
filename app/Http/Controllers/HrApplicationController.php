<?php

namespace App\Http\Controllers;

use App\HrApplication;
use App\HumanResources;
use Illuminate\Support\Facades\Storage;
use Validator;
use Redirect;
use Response;
use DataTables;
use Illuminate\Http\Request;
use Log;

class HrApplicationController extends Controller
{

//first trial
    public
    function showHrApp()
    {
        return view('job_application');
    }
//    start application view
    public function showProfile()
    {
        return view('applicant_profile_details');
    }

    public
    function findAllApplicants()
    {
        $data = HrApplication::all();
        return view('job_applicant_display')->with('data', $data);
    }

    public function applicants()
    {
        $data = HrApplication::all();
        return Datatables::of($data)
            ->addColumn('delete', function ($e) {
                return '<button class="btn btn-xs btn-primary" id="edit"><i class="glyphicon glyphicon-trash"></i>Delete</a>';
            })
            ->addColumn('download', function ($e) {
                return '<a href="applicants_cv/'.$e->cv.'" target="_blank">
                <button class="btn"><i class="fa fa-download"></i> Download CV</button></a>';
            })
                ->addColumn('passport', function ($e) {
                    return '<img src="passports/'.$e->passport.'" style="width:128px;height:110px;"></a>';
                })
            ->rawColumns(['delete','download','passport'])
            ->make(true);
    }

    public function download($cv)
    {

        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/cv/".$cv;

        return Response::download($file,'cv_'.$cv);
    }
    public function delete($id)
    {
        HrApplication::where('id', $id)->delete();
        return view('job_applicant_display')->with('data');
    }

}
