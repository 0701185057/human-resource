<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
class LoginController extends Controller
{
//    public function login(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'email' => ['required', 'string', 'email', 'max:50'],
//            'password' => ['required', 'string', 'min:8']
//        ]);
//
//        if ($validator->fails()) {
//            return json_encode($validator->errors());
//        }
//        $data =User::where('email', $request->email)->where('password', $request->password)->first();
//        if ($data == null) {
//            return view('login');
//        } else return view('add_employee');
//    }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
