<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\HumanResources;
use Validator;
use Illuminate\Http\Request;
use Redirect;
use Auth;
use Log;

class HumanResourcesController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:25'],
            'middle_name' => ['sometimes', 'string', 'max:25'],
            'last_name' => ['required', 'string', 'max:25'],
            'email' => ['required', 'email', 'max:255', 'unique:human_resources'],
            'phone_number' => ['required', 'string', 'max:10', 'max:14', 'unique:human_resources'],
            'id_number' => ['required', 'string', 'min:7', 'max:8', 'unique:human_resources'],
            'staff_number' => ['required', 'string', 'min:3', 'unique:human_resources'],
        ]);

        if ($validator->fails()) {
            return redirect('home')
                ->withErrors($validator)
                ->withInput();
        }
        $data = new HumanResources();
        $data->first_name = $request->first_name;
        $data->middle_name = $request->middle_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->phone_number = $request->phone_number;
        $data->id_number = $request->id_number;
        $data->staff_number = $request->staff_number;
        $data->created_by = Auth::user()->id;
        $data->save();
        return redirect::to('home');
    }

    public function displayAddmployeePage()
    {
        return view('add_employee');
    }

    public function showProfile()
    {
        return view('profile');
    }

    public function fetchAll()
    {
        return HumanResources::all();
        return view('edit')->with('unit', $unit);
    }

    public function findById($id)
    {
        return HumanResources::find($id);
    }

    public function update(Request $request, $id)
    {
//        Log::info($request);
        $data = $this->findById($id);
        $data->first_name = $request->first_name;
        $data->middle_name = $request->middle_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->phone_number = $request->phone_number;
        $data->id_number = $request->id_number;
        $data->staff_number = $request->staff_number;
        $data->save();
    }

    public function editEmployee($id)
    {
        $data = HumanResources::find($id);
        return view('edit')->with('d', $data);
    }

    public function destroy($id)
    {
        HumanResources::where('id', $id)->delete();

        return Redirect::to('home')->with('success', 'Product deleted successfully');
    }

    public function profileSearchById($id)
    {
        $data = RegisterController::find($id);
        return view('profile')->with('d', $data);
    }

    public function fetchAllApplicants()
    {
        $data = HumanResources::all();
        return view('job_applicant')->with('d', $data);
    }
}
