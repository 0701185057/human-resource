<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicantLoginController extends Controller
{
    public function showApplicantLogin()
    {
        return view('applicant_login');
    }
}
