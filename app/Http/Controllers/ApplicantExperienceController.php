<?php

namespace App\Http\Controllers;

use App\ApplicantExperience;
use App\ApplicantRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Redirect;
use Response;
use DataTables;

class ApplicantExperienceController extends Controller
{
    public function showExperience()
    {
        return view('experience');

    }

    public function showAddExperience()
    {
        return view('add_work_experience');

    }

    public function createExperienceOfApplicant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employer' => ['required', 'string'],
            'job_title' => ['required', 'string'],
            'start_date' => ['required'],
            'end_date' => ['nullable'],
            'achievements' => ['required', 'string'],
            'awards'=>'required|max:1024',
        ]);
        if ($validator->fails()) {
            return redirect::to('experience')->withErrors($validator);
        }
        $award_attachments = null;
        if ($doc = $request->file('awards')) {
            $destinationPath = 'awards/'; // upload path
            $curriculum_vitae = date('YmdHis') . "." . $doc->getClientOriginalExtension();
            $doc->move($destinationPath, $award_attachments);
        }
        $data = new ApplicantExperience();
        $data->employer = $request->employer;
        $data->job_title = $request->job_title;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->achievements = $request->achievements;
        $data->applicant = Auth::guard('applicant')->user()->id;
        $data->awards = $award_attachments;
        $data->save();
        return view('experience')->with('d', $data);
    }

    public function showAttachment($attachment)
    {
        $file = public_path() . "/cv/" . $attachment;
        return response()->file($file);
    }
    public function findByid($id)
    {
        return ApplicantExperience::find($id);
    }
    public function updateExperienceOfApplicant(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'employer' => ['required', 'string'],
            'job_title' => ['required', 'string'],
            'start_date' => ['required'],
            'end_date' => ['nullable'],
            'achievements' => ['required', 'string'],
            'awards' => 'nullable|mimes:pdf,jpeg,png,jpg,zip|max:4000',
        ]);
        if ($validator->fails()) {
            return json_encode($validator->errors());
            return redirect::to('summary')->withErrors($validator);
        }
        $award_attachments = null;
        if ($doc = $request->file('awards')) {
            $destinationPath = 'awards/'; // upload path
            $curriculum_vitae = date('YmdHis') . "." . $doc->getClientOriginalExtension();
            $doc->move($destinationPath, $award_attachments);
        }
        $data = $this->findById($id);
        $data->employer = $request->employer;
        $data->job_title = $request->job_title;
        $data->start_date = $request->start_date;
        $data->end_date = $request->end_date;
        $data->achievements = $request->achievements;
        $data->applicant = Auth::guard('applicant')->user()->id;
        $data->awards = $award_attachments;
        $data->save();
        return redirect::to('summary')->with('information', $this->getApplicantById($data->applicant));
    }
    public function getApplicantById($id){
        return ApplicantRegistration::with('profile', 'education', 'experience', 'referee')
            ->where('id', $id)->first();
    }
    public function destroy($id)
    {
        ApplicantExperience::where('id', $id)->delete();

        return Redirect::to('summary')->with('success', 'Product deleted successfully');
    }
}
