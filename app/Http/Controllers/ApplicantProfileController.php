<?php

namespace App\Http\Controllers;

use App\ApplicantProfile;
use App\ApplicantRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Redirect;
use Response;
use DataTables;
use Log;

class ApplicantProfileController extends Controller
{
    public function showApplicationProfile()
    {
        return view('applicant_profile_details');
    }
    public function createProfileOfApplicant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'first_name' => ['required', 'string', 'max:75'],
            'last_name' => ['required', 'string', 'max:75'],
            'gender' => ['required'],
            'dob' => ['required'],
            'phone_number' => ['required','min:10', 'max:14'],
            'alt_phone' => ['required','min:10', 'max:14'],
            'postal_address' => ['required', 'string', 'max:25'],
            'postal_code' => ['required', 'string', 'max:25'],
        ]);
        if ($validator->fails()) {
//            return json_encode($validator->errors());
            return redirect::to('applicant_profile_details')->withErrors($validator);
        }
        $data = new ApplicantProfile();
        $data->title = $request->title;
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->gender = $request->gender;
        $data->dob = $request->dob;
        $data->phone = $request->phone_number;
        $data->applicant = Auth::guard('applicant')->user()->id;
        $data->alt_phone = $request->alt_phone;
        $data->postal_address = $request->postal_address;
        $data->postal_code = $request->postal_code;
        $data->save();
        return view('applicant_profile_details')->with('d', $data);
    }
    public function findById($id)
    {
    return ApplicantProfile::find($id);
    }

    public function update(Request $request, $id)
    {
        $data = $this->findById($id);
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->gender = $request->gender;
        $data->dob = $request->dob;
        $data->phone= $request->phone;
        $data->alt_phone = $request->alt_phone;
        $data->postal_address = $request->postal_address;
        $data->postal_code = $request->postal_code;
        $data->save();

//        Log::info($data);

//        find parent using parent id(in the relationship)
        $parent=ApplicantRegistration::find($data->applicant);
//        assign new values to first name and last name
        $parent->first_name = $request->first_name;
        $parent->last_name = $request->last_name;
//        save
        $parent->save();
    }
    public function destroy($id)
    {
        ApplicantProfile::where('id', $id)->delete();

        return Redirect::to('summary')->with('success', 'Product deleted successfully');
    }
}
