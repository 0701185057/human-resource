<?php

namespace App\Http\Controllers;

use App\ApplicantEducation;
use App\ApplicantRegistration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use Validator;
use Redirect;
use Response;
use DataTables;
use Log;

class ApplicantRegistrationController extends Controller
{
    public function showApplicantRegistration()
    {
        return view('applicant_registration');
    }

    public function createRegistrationOfApplicant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:25'],
            'last_name' => ['required', 'string', 'max:25'],
            'email' => ['required', 'email', 'max:75','unique:applicant_registrations'],
            'password' => ['required', 'string', 'min:8'],
            'confirm_password' => ['same:password'],
        ]);
        if ($validator->fails()) {
//            return json_encode($validator->errors());
            return redirect::to('applicant_registration')->withErrors($validator);
        }
        $data = new ApplicantRegistration();
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->save();
        return view('applicant_registration')->with('d', $data);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::guard('applicant')->attempt($credentials)) {
            // Authentication passed...
            return view('applicant_profile_details')->with('message', 'The success message!');
        } else {
            return view('applicant_login')->withErrors($credentials);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/applicant_login')->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
    }

    public function allInfo()
    {

        $data = ApplicantRegistration::with('profile', 'education', 'experience', 'referee')->get();
        return view('hr_applicant_display')->with('information', $data);
    }

    public function showSummary()
    {
        $id = Auth::guard('applicant')->user()->id;
//        Log::info($id);
        $information = $this->getApplicantById( $id);
//        Log::info($information);
        return view('summary')->with('information', $information);
    }

    public function getApplicantById($id){
        return ApplicantRegistration::with('profile', 'education', 'experience', 'referee')
            ->where('id', $id)->first();
    }

    public function showApplicantsBlade(){
        return view('hr_applicant_display');
    }
    public
    function findAllApplicants()
    {
        $data = ApplicantRegistration::all();
        return view('hr_applicant display')->with('data', $data);
    }
    public function delete($id)
    {
        ApplicantRegistration::with('profile', 'education', 'experience', 'referee')->delete();
        return view('hr_applicant_display')->with('data');
    }
    public function showCv($id)
    {
        $information =$this->getApplicantById($id);
        return view('curriculumvitae')->with('information', $information);
    }
}
