<?php

namespace App\Http\Controllers;

use App\ApplicantEducation;
use App\ApplicantRegistration;
use Illuminate\Http\Request;
use Validator;
use Redirect;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DataTables;

class ApplicantEducationController extends Controller
{
    public function showEducation()
    {
        return view('education');
    }
    public function showAddEducation()
    {
        return view('add_education');
    }
    public function createEducationOfApplicant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'institution' => ['required', 'string', 'max:100'],
            'program' => ['required','string',],
            'level' => ['required', 'string'],
            'grade' => ['required', 'string'],
            'startdate' => ['required'],
            'attachment'=>'nullable|mimes:pdf,jpeg,png,jpg,zip|max:4000',
        ]);
        if ($validator->fails()) {
            return redirect::to('add_education')->withErrors($validator);
        }
        $curriculum_vitae = null;
        if ($files = $request->file('attachment')) {
            $destinationPath = 'cv/'; // upload path
            $curriculum_vitae = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $curriculum_vitae);
        }
        $data = new ApplicantEducation();
        $data->institution = $request->institution;
        $data->program = $request->program;
        $data->level = $request->level;
        $data->grade = $request->grade;
        $data->startdate = $request->startdate;
        $data->enddate= $request->enddate;
        $data->attachment = $curriculum_vitae;
        $data->applicant = Auth::guard('applicant')->user()->id;
        $data->save();
        return view('education')->with('d', $data);
    }

    public function showAttachment($attachment){
        $file= public_path(). "/cv/".$attachment;
        return response()->file($file);
    }
    public function findByid($id)
    {
        return ApplicantEducation::find($id);
    }
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
        'institution' => ['required', 'string', 'max:100'],
        'program' => ['required','string',],
        'level' => ['required', 'string'],
        'grade' => ['required', 'string'],
        'startdate' => ['required'],
        'enddate' => ['required'],
        'attachment.*'=>'nullable|file|mimes:pdf|max:4000',
    ]);
        if ($validator->fails()) {
            return json_encode($validator->errors());
            return redirect::to('summary')->withErrors($validator);
        }
        $curriculum_vitae = null;
        if ($files = $request->file('attachment')) {
            $destinationPath = 'cv/'; // upload path
            $curriculum_vitae = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $curriculum_vitae);
        }
        $data = $this->findById($id);
        $data->institution = $request->institution;
        $data->program = $request->program;
        $data->level = $request->level;
        $data->grade = $request->grade;
        $data->startdate = $request->startdate;
        $data->enddate= $request->enddate;
        $data->attachment = $curriculum_vitae;
        $data->save();
        return redirect::to('summary')->with('information', $this->getApplicantById($data->applicant));
    }
//data from registration table
    public function getApplicantById($id){
        return ApplicantRegistration::with('profile', 'education', 'experience', 'referee')
            ->where('id', $id)->first();
    }
    public function destroy($id)
    {
        ApplicantEducation::where('id', $id)->delete();

        return Redirect::to('summary')->with('success', 'Product deleted successfully');
    }
}
