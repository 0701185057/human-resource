<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ApplicantRegistration extends Authenticatable
{
    protected $hidden = [
        'password'
    ];
    public function profile()
    {
        return $this->hasOne(ApplicantProfile::class,'applicant','id');
    }
    public function education()
    {
        return $this->hasMany(ApplicantEducation::class,'applicant','id');
    }
    public function experience()
    {
        return $this->hasMany(ApplicantExperience::class,'applicant','id');
    }
    public function referee()
    {
        return $this->hasMany(ApplicantReferee::class,'applicant','id');
    }
}

