<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicantReferee extends Model
{
    public function referee() {

        return $this->belongsTo(ApplicantRegistration::class,'created_by','id');
    }
}
