<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantRefereesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_referees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company');
            $table->string('job_title');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('postal_address');
            $table->string('postal_code');
            $table->bigInteger('applicant')->unsigned();;
            $table->foreign('applicant')->references('id')->on('applicant_registrations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_referees');
    }
}
