<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_education', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('institution');
            $table->string('program');
            $table->string('level');
            $table->string('grade');
            $table->string('startdate');
            $table->string('enddate');
            $table->string('attachment')->nullable();
            $table->bigInteger('applicant')->unsigned();
            $table->foreign('applicant')->references('id')->on('applicant_registrations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_education');
    }
}
