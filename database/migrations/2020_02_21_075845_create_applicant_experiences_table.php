<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_experiences', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('employer');
                $table->string('job_title');
                $table->string('start_date');
                $table->string('end_date');
                $table->string('achievements');
                $table->string('awards')->nullable();
                $table->bigInteger('applicant')->unsigned();;
                $table->foreign('applicant')->references('id')->on('applicant_registrations');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_experiences');
    }
}
