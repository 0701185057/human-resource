@extends('layouts.header_without_nav')

@section('content')
    <div class="well"><h3 text-align="center">Register here</h3>
        <div class="card">

            <div class="card-body">
                <div class="well">
                    <form method="POST" action="/register">
                        @csrf
                        <div class="form-group row">
                            <label for="First Name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                            <div class="col-md-4">
                                <input id="first_name" type="text" class="form-control ('first_name')" name="first_name"
                                       value="{{ old('first_name') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="middle_name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>
                            <div class="col-md-4">
                                <input id="middle_name" type="text" class="form-control ('middle_name')  "
                                       name="middle_name" value="{{ old('middle_name') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-4">
                                <input id="last_name" type="text" class="form-control ('last_name') " name="last_name"
                                       value="{{ old('last_name') }}" required autocomplete="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-4">
                                <input id="email" type="email" class="form-control ('email') " name="email"
                                       value="{{ old('email') }}" required autocomplete="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                            <div class="col-md-4">
                                <input id="phone_number" type="text" class="form-control ('phone_number')"
                                       name="phone_number" value="{{ old('phone_number') }}" required
                                       autocomplete="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_number"
                                   class="col-md-4 col-form-label text-md-right">{{ __('ID Number') }}</label>

                            <div class="col-md-4">
                                <input id="staff_number" type="text" class="form-control ('id_number') "
                                       name="id_number" value="{{ old('id_number') }}" required
                                       autocomplete="id_number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Staff Number') }}</label>

                            <div class="col-md-4">
                                <input id="staff_number" type="text" class="form-control ('staff_number') "
                                       name="staff_number" value="{{ old('staff_number') }}" required
                                       autocomplete="staff_number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="col-md-4">
                                <input id="password" type="password" class="form-control ('password')  "
                                       name="password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-4">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                                <a class="btn btn-link" href="{{ route('login') }}">
                                    {{ __('If already registered here?') }}</a>
                                <div>
                                </div>
                            </div>
                        </div>
                        {{--                        @if(count($errors))--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <div class="alert alert-danger">--}}
                        {{--                                    <ul>--}}
                        {{--                                        @foreach($errors->all() as $error)--}}
                        {{--                                            <li>{{$error}}</li>--}}
                        {{--                                        @endforeach--}}
                        {{--                                    </ul>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        @endif--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
