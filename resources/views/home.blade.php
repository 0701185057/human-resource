@extends('modal')

@section('content')
    <div class="row-fluid">
        <div class="card">
            <!-- Button to Open the Modal -->
            <button type="button" class="btn btn-light text-primary" data-toggle="modal" data-target="#myModal">
                Add new employee
            </button>
        </div>
        <div class="card">
            <!-- The Modal -->
            <div class="modal" id="myModal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">ADD THE NEW EMPLOYEE TO THE SYSTEM</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <form class="form-group" id="add_employees">
                                @csrf

                                <div class="form-group row">
                                    <label for="First Name"
                                           class="col-md-4 col-form-label text-md-right">First Name</label>

                                    <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="name" autofocus>
                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="ame"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="middle_name" type="text"
                                               class="form-control @error('middle_name') is-invalid @enderror"
                                               name="middle_name" value="{{ old('middle_name') }}" required
                                               autocomplete="name">

                                        @error('middle_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="last_name" type="text"
                                               class="form-control @error('last_name') is-invalid @enderror"
                                               name="last_name" value="{{ old('last_name') }}" required
                                               autocomplete="name">
                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email"
                                               value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="phone_number" type="number"
                                               class="form-control @error('phone_number') is-invalid @enderror"
                                               name="phone_number" value="{{ old('phone_number') }}" required
                                               autocomplete="name">

                                        @error('phone_number')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Id Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="id_number" type="number"
                                               class="form-control @error('id_number') is-invalid @enderror"
                                               name="id_number" value="{{ old('id_number') }}" required
                                               autocomplete="phone_number">

                                        @error('id_number')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Staff Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="staff_number" type="text"
                                               class="form-control @error('staff_number') is-invalid @enderror"
                                               name="staff_number" value="{{ old('staff_number') }}" required
                                               autocomplete="staff_number">

                                        @error('staff_number')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" id="insert" class="btn btn-primary">
                                                INSERT
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" id class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table display" id="employees">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Middle Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email Address</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Id number</th>
            <th scope="col">Staff Number</th>
            <th scope="col">Create by</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        {{--  second modal   for update   --}}
        <div class="modal" id="exampleModalLong">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">UPDATE THE EMPLOYEES DETAILS HERE</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="modal-body">
                                <input id="id" value="id" type="text" name="id" hidden>
                                <div class="form-group row">
                                    <label for="First Name"
                                           class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>
                                    <div class="col-md-6">
                                        <input id="fname" type="name"
                                               class="form-control @error('first_name') is-invalid @enderror"
                                               name="first_name" value="$('#first_name').first_name"
                                               required
                                               autocomplete="name" autofocus>

                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="ame"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Middle Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="mname" value="middle_name" type="text"
                                               class="form-control @error('middle_name') is-invalid @enderror"
                                               name="middle_name" value="{{ old('middle_name') }}"
                                               required
                                               autocomplete="name">

                                        @error('middle_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lname"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="lname" value="last_name" type="text"
                                               class="form-control @error('last_name') is-invalid @enderror"
                                               name="last_name" value="{{ old('last_name') }}" required
                                               autocomplete="name">

                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="mail" value="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email"
                                               value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="pnumber"
                                               type="name"
                                               class="form-control @error('phone_number') is-invalid @enderror"
                                               name="name" value="{{ old('phone_number') }}"
                                               required
                                               autocomplete="name">

                                        @error('phone_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Id Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="id_no" value="{{ ('id_number') }}" type="name"
                                               class="form-control @error('id_number') is-invalid @enderror"
                                               name="id_number" value="{{ old('id_number') }}" required
                                               autocomplete="phone_number">

                                        @error('id_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staff_number"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Staff Number') }}</label>

                                    <div class="col-md-6">
                                        <input id="snumber"
                                               value=staff_number
                                               type="name"
                                               class="form-control @error('staff_number') is-invalid @enderror"
                                               name="staff_number" value="{{ old('staff_number') }}"
                                               required
                                               autocomplete="staff_number">

                                        @error('staff_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" id="update" class="btn btn-primary">
                                                UPDATE
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        </body>
    </table>
    </div>
    <script>
        $(document).ready(function()  {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            var table = $('#employees').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{{url('/all_employees')}}',
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'first_name', id: 'first_name'},
                    {data: 'middle_name', id: 'middle_name'},
                    {data: 'last_name', id: 'last_name'},
                    {data: 'email', id: 'email'},
                    {data: 'phone_number', id: 'phone_number'},
                    {data: 'id_number', id: 'id_number'},
                    {data: 'staff_number', id: 'staff_number'},
                    {data: 'created_by', id: 'created_by'},
                    {data: 'edit', id: 'edit'},
                    {data: 'delete', id: 'delete'}
                ], dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $('#employees').on('click', '#edit', function (e) {
                e.preventDefault();
                var data = table.row($(this).parents('tr')).data();
                $('#id').val(data.id);
                $('#fname').val(data.first_name);
                $('#mname').val(data.middle_name);
                $('#lname').val(data.last_name);
                $('#mail').val(data.email);
                $('#pnumber').val(data.phone_number);
                $('#id_no').val(data.id_number);
                $('#snumber').val(data.staff_number);
                $("#exampleModalLong").modal('show');
            });

            $("#insert").click(function (e) {
                e.preventDefault();
                // log.console("first_name");
                var first_name = $("input[id=first_name]").val();
                var middle_name = $("input[id=middle_name]").val();
                var last_name = $("input[id=last_name]").val();
                var email = $("input[id=email]").val();
                var phone_number = $("input[id=phone_number]").val();
                var id_number = $("input[id=id_number]").val();
                var staff_number = $("input[id=staff_number]").val();
                $.ajax({
                    type: 'POST',
                    url: '/add_employee',
                    data: {
                        first_name: first_name,
                        middle_name: middle_name,
                        last_name: last_name,
                        email: email,
                        phone_number: phone_number,
                        id_number: id_number,
                        staff_number: staff_number
                    },
                    success: function (data) {
                        alert("one reocord added succesfully");
                        $('#myModal').modal('hide');
                        table.ajax.reload(null, false);
                    }, error: function (data) {
                        alert("Error try again");
                        $('#insert').html('RE-INSERT')
                    }
                });
            });

            $('#update').click(function (e) {
                e.preventDefault();

                $('#update').html('SENDING...')
                // console.log("first_name");
                var id = $("input[id=id]").val();
                var first_name = $("input[id=fname]").val();
                var middle_name = $("input[id=mname]").val();
                var last_name = $("input[id=lname]").val();
                var email = $("input[id=mail]").val();
                var phone_number = $("input[id=pnumber]").val();
                var id_number = $("input[id=id_no]").val();
                var staff_number = $("input[id=snumber]").val();
                $.ajax({

                    type: 'POST',

                    url: "{{url('/edit/')}}/" + id,

                    data: {

                        first_name: first_name,
                        middle_name: middle_name,
                        last_name: last_name,
                        email: email,
                        phone_number: phone_number,
                        id_number: id_number,
                        staff_number: staff_number
                    },

                    success: function (data) {
                        alert("Success");
                        $("#exampleModalLong").modal('hide');
                        $("#exampleModalLong")[0].reset();
                        table.ajax.reload(null, false);
                        $('#update').html('UPDATING..')
                    }, error: function (data) {
                        alert("Error try again");
                        $('#update').html('RE-SEND')
                    }
                });

            });

            $("#employees").on('click', '#delete', function (e) {
                e.preventDefault();
                var data = table.row($(this).parents('tr')).data();
                // console.log(data.id)
                $.ajax(
                    {
                        type: 'GET',
                        url: "/delete/" + data.id,
                        success: function () {
                            alert("Record deleted");
                            table.ajax.reload(null, false);
                        }, error: function (data) {
                            alert("Error try again");
                        }
                    });

            });
        });
    </script>
@endsection
