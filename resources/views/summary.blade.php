@extends('application_form_header')

@section('content')
    {{ csrf_field() }}
    <div class="container">
        <div class="card">
            <div class="row justify-content-center align-items-center m-4">
                <div class="col col-sm-12 align-self-center">
                    <div class="row">
                        <div class="col">
                            <div>
                            </div>
                            <p class="font-weight-bold">PERSONAL PROFILE</p>
                            <hr>
                            @if($information != null && $information->profile != null)

                                FIRST NAME:&nbsp{{$information->profile->first_name}}
                                <br>
                                LAST NAME:&nbsp{{$information->profile->last_name}}
                                <br>
                                GENDER:&nbsp{{$information->profile->gender}}
                                <br>
                                DOB:&nbsp{{$information->profile->dob}}
                                <br>
                                PHONE NUMBER:&nbsp{{$information->profile->phone}}
                                <br>
                                ALT PHONE NUMBER:&nbsp{{$information->profile->alt_phone}}
                                <br>
                                POSTAL ADDRESS:&nbsp{{$information->profile->postal_address}}
                                <br>
                                POSTAL CODE:&nbsp{{$information->profile->postal_code}}
                                <br>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                        data-target="#profileModal">
                                    EDIT
                                </button>

                        <!-- Modal for personal profile-->
                            <div class="modal fade" id="profileModal" tabindex="-1" role="dialog"
                                 aria-labelledby="profileModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="profileModalLabel">Edit here</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <input type="text" class="form-control" id="profile_id" hidden
                                                       value="{{$information->profile->id}}">
                                                <div class="form-group row">
                                                    <label for="first_name" class="col-sm-4 col-form-label">FIRST
                                                        NAME:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="fname"
                                                               value="{{$information->profile->first_name}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="last_name" class="col-sm-4 col-form-label">LAST
                                                        NAME:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="lname"
                                                               value="{{$information->profile->last_name}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="gender" class="col-sm-4 col-form-label">GENDER:</label>
                                                    <div class="col-sm-8">
                                                        <input type="gender" class="form-control" id="gender"
                                                               value="{{$information->profile->gender}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="dob" class="col-sm-4 col-form-label">DOB:</label>
                                                    <div class="col-sm-8">
                                                        <input type="date" class="form-control" id="dob"
                                                               value="{{$information->profile->dob}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="phone" class="col-sm-4 col-form-label">PHONE
                                                        NUMBER:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="phone"
                                                               value="{{$information->profile->phone}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="alt_phone" class="col-sm-4 col-form-label">ALTERNATIVE
                                                        NUMBER:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="alt_phone"
                                                               value="{{$information->profile->alt_phone}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="postal_address" class="col-sm-4 col-form-label">POSTAL
                                                        ADDRESS:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="postal_address"
                                                               value="{{$information->profile->postal_address}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="postal_code" class="col-sm-4 col-form-label">POSTAL
                                                        CODE:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="postal_code"
                                                               value="{{$information->profile->postal_code}}">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="edit-modal btn btn-secondary"
                                                    data-dismiss="modal">Close
                                            </button>
                                            <button type="button" id="update" class="btn btn-primary">Save changes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            &nbsp &nbsp
                            {{--                            <a href="/delete_profile" class="btn btn-secondary float-right">DELETE</a>--}}
                            <hr>
                            <p class="font-weight-bold">EDUCATION BACKGROUND</p>
                            <hr>
                            @foreach($information->education as $e)
                                INSTITUTION:&nbsp{{$e->institution}}
                                <br>
                                PROGRAM:&nbsp{{$e->program}}
                                <br>
                                LEVEL:&nbsp{{$e->level}}
                                <br>
                                GRADE:&nbsp{{$e->grade}}
                                <br>
                                STARTED AT:{{$e->startdate}}
                                <br>
                                END AT:&nbsp{{$e->enddate}}
                                <br>ATTACHMENT:&nbsp&nbsp<a href="/attachment/{{$e->attachment}}"
                                                            class="btn btn-secondary">view uploaded attachment here</a>
                                <br>
                                <br>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                        data-target="#e{{$e->id}}">
                                    EDIT
                                </button>
                                &nbsp &nbsp
                                <a href="/delete_education/{{$e->id}}" class="btn btn-secondary float-right">DELETE</a>
                                <hr>
                                <!-- Modal for work education-->
                                <div class="modal fade" id="e{{$e->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="educationModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="educationModalLabel">Edit education</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" action="/update_education/{{$e->id}}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group row">
                                                        <label for="institution" class="col-sm-4 col-form-label">INSTITUTION:</label>
                                                        <div class="col-sm-8"><input type="text" name="institution"
                                                                                     class="form-control"
                                                                                     id="institution"
                                                                                     value="{{$e->institution}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="program"
                                                               class="col-sm-4 col-form-label">PROGRAM: </label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="program"
                                                                   name="program" value="{{$e->program}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="level"
                                                               class="col-sm-4 col-form-label">LEVEL: </label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="level"
                                                                   name="level" value="{{$e->level}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="grade"
                                                               class="col-sm-4 col-form-label">GRADE:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="grade"
                                                                   name="grade" value="{{$e->grade}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="startdate" class="col-sm-4 col-form-label">STARTED
                                                            AT:</label>
                                                        <div class="col-sm-8">
                                                            <input type="date" class="form-control" id="startdate"
                                                                   name="startdate" value="{{$e->startdate}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="enddate" class="col-sm-4 col-form-label">END
                                                            AT:</label>
                                                        <div class="col-sm-8">
                                                            <input type="date" class="form-control" id="enddate"
                                                                   name="enddate" value="{{$e->enddate}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="attachment" class="col-sm-4">ATTACHMENT:
                                                            (Scanned
                                                            copies of certifications)</label>
                                                        <input name="attachment" type="file" id="attachment">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">
                                                            Close
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">Save changes
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <hr>
                            @endforeach
                            <p class="font-weight-bold"> WORK EXPERIENCE:</p>
                            @foreach($information->experience as $e)
                                <hr>
                                EMPLOYER:&nbsp{{$e->employer}}
                                <br>
                                JOB TITLE:&nbsp{{$e->job_title}}
                                <br>
                                START DATE:&nbsp{{$e->start_date}}
                                <br>
                                END DATE:&nbsp{{$e->end_date}}
                                <br>
                                ACHIEVEMENTS:&nbsp{{$e->achievments}}
                                <br>
                                AWARDS:&nbsp&nbsp&nbsp<a href="/attachment/{{$e->award}}" class="btn btn-secondary">view
                                    uploaded award here</a>
                                <br>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                        data-target="#experienceModal{{$e->id}}">
                                    EDIT
                                </button>
                                <!-- Modal for work experience-->
                                <div class="modal fade" id="experienceModal{{$e->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="experienceModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="experienceModalLabel{{$e->id}}">Edit
                                                    experience</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" action="/update_experience/{{$e->id}}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group row">
                                                        <label for="employer"
                                                               class="col-sm-4 col-form-label">EMPLOYER:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="employer"
                                                                   name="employer" value="{{$e->employer}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="job_title" class="col-sm-4 col-form-label">JOB
                                                            TITLE</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="job_title"
                                                                   id="job_title"
                                                                   value="{{$e->job_title}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="start_date" class="col-sm-4 col-form-label">START
                                                            DATE:</label>
                                                        <div class="col-sm-8">
                                                            <input type="date" class="form-control"
                                                                   name="start_date" id="start_date"
                                                                   value="{{$e->start_date}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="end_date" class="col-sm-4 col-form-label">END
                                                            DATE:</label>
                                                        <div class="col-sm-8">
                                                            <input type="date" class="form-control" name="end_date"
                                                                   id="end_date"
                                                                   value="{{$e->end_date}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="achievements" class="col-sm-4 col-form-label">ACHIEVEMENTS:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="achievements"
                                                                   name="achievements" value="{{$e->achievements}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="award" class="col-sm-4">AWARDS: (Scanned copies of
                                                            certifications)</label>
                                                        <input name="award " type="file" id="award">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">
                                                            Close
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">Save changes
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="/delete_experience/{{$e->id}}" class="btn btn-secondary float-right">DELETE</a>
                                <br>
                            @endforeach
                            <hr>
                            <p class="font-weight-bold">REFEREE:</p>
                            <hr>
                            @foreach($information->referee as $e)
                                <br>
                                COMPANY:&nbsp{{$e->company}}
                                <br>
                                NAME:&nbsp{{$e->name}}
                                <br>
                                TITLE:&nbsp{{$e->job_title}}
                                <br>
                                PHONE NUMBER:&nbsp{{$e->phone}}
                                <br>
                                EMAIL:&nbsp{{$e->email}}
                                <br>
                                POSTAL ADDRESS:&nbsp{{$e->postal_address}}
                                <br>
                                POSTAL CODE:&nbsp{{$e->postal_code}}
                                <br>
                                <br>
                                <!-- Button trigger modal-- start referee-->
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                        data-target="#refereeModal{{$e->id}}">
                                    EDIT
                                </button>

                                <!-- Modal referee-->
                                <div class="modal fade" id="refereeModal{{$e->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="refereeModalLabel{{$e->id}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="refereeModalLabel">Edit referee</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" action="/update_referee/{{$e->id}}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group row">
                                                        <label for="company"
                                                               class="col-sm-4 col-form-label">COMPANY:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="company"
                                                                   name="company" value="{{$e->company}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-4 col-form-label">NAME:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="name"
                                                                   name="name" value="{{$e->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="job_title"
                                                               class="col-sm-4 col-form-label">TITLE:</label>
                                                        <div class="col-sm-8">
                                                            <input type="job_title" class="form-control" id="job_title"
                                                                   name="job_title" value="{{$e->job_title}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="phone" class="col-sm-4 col-form-label">PHONE
                                                            NUMBER:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="phone"
                                                                   name="phone" value="{{$e->phone}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="email" class="col-sm-4 col-form-label">
                                                            EMAIL:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="email"
                                                                   name="email" value="{{$e->email}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="postal_address" class="col-sm-4 col-form-label">POSTAL
                                                            ADDRESS:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="postal_address"
                                                                   name="postal_address" value="{{$e->postal_address}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="postal_code" class="col-sm-4 col-form-label">POSTAL
                                                            CODE:</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="postal_code"
                                                                   name="postal_code" value="{{$e->postal_code}}">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">
                                                            Close
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">Save changes
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="/delete_referee/{{$e->id}}" class="btn btn-secondary float-right">DELETE</a>
                                <br>
                                <hr>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $("#update").click(function (e) {
                e.preventDefault();
                $('#update').html('SENDING...')
                var id = $("input[id=profile_id]").val();

                var first_name = $("input[id=fname]").val();
                var last_name = $("input[id=lname]").val();
                var gender = $("input[id=gender]").val();
                var dob = $("input[id=dob]").val();
                var phone = $("input[id=phone]").val();
                var alt_phone = $("input[id=alt_phone]").val();
                var postal_address = $("input[id=postal_address]").val();
                var postal_code = $("input[id=postal_code]").val();
                // console.log($("input[id=postal_code]").val());
                $.ajax({

                    type: 'POST',


                    url: "{{url('/update/')}}/" + id,

                    data: {

                        first_name: first_name,
                        last_name: last_name,
                        gender: gender,
                        dob: dob,
                        phone: phone,
                        alt_phone: alt_phone,
                        postal_address: postal_address,
                        postal_code: postal_code
                    },

                    success: function (data) {
                        alert("Success");
                        $("#profileModal").modal('hide');
                        $("#profileModal")[0].reset();
                        table.ajax.reload(null, false);
                        $('#update').html('UPDATING..')
                    }, error: function (data) {
                        alert("Error try again");
                        $('#update').html('RE-SEND')
                    }
                });

            });
        });
    </script>
@endsection
