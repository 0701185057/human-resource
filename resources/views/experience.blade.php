@extends('application_form_header')

@section('content')
    <div class="container">
        <div class="card">
        <div class="row justify-content-center align-items-center m-4">
            <div class="col col-sm-12 align-self-center">
                <form method="POST" action="/experience" id="workform">
                    {{ csrf_field() }}
                    <div class="form-group col-sm-6">
                        <p>(Fields marked * are required)</p>
                    </div>

                    <!-- Employer Field -->
                    <div class="form-group col-sm-6">
                        <label for="employer">Employer*:</label>
                        <input class="form-control" name="employer" type="text" id="employer">

                    </div>

                    <!-- Title Field -->
                    <div class="form-group col-sm-6">
                        <label for="title">Job Title* (Designation):</label>
                        <input class="form-control" name="job_title" type="text" id="title">
                    </div>

                    <!-- Start Date Field -->
                    <div class="form-group col-sm-6">
                        <label for="start_date">Start Date*:</label>
                        <input class="form-control" id="startdate" data-toggle="datetimepicker" data-target="#startdate"
                               name="start_date" type="text">
                    </div>

                    <!-- End Date Field -->
                    <div class="form-group col-sm-6">
                        <label for="end_date">End Date:</label>
                        <input class="form-control" id="enddate" data-toggle="datetimepicker" data-target="#enddate"
                               name="end_date" type="text">
                    </div>

                    <!-- Achievements Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        <label for="achievements">Roles and Achievements*:</label>
                        <textarea class="summernote form-control " name="achievements" cols="50" rows="10"
                                  id="achievements"></textarea>
                    </div>

                    <!-- Attachment Field -->
                    <div class="form-group col-sm-6">
                        <label for="awards">Attachment: (Scanned copies of awards)</label>
                        <input name="awards" type="file" id="awards">
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <hr>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save & go back to list
                        </button>
                        <button type="button" class="btn btn-secondary" onClick="submitNewForm()">Save & Add New Entry
                        </button>
                        <a href="" class="btn btn-outline-warning">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
                <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
                <script
                    src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
                <script type="text/javascript">
                    $(function () {
                        $('#startdate').datetimepicker({
                            format: 'YYYY-MM-DD',
                            useCurrent: false,
                            maxDate: moment()
                        });

                        $('#enddate').datetimepicker({
                            format: 'YYYY-MM-DD',
                            useCurrent: false,
                            maxDate: moment()
                        });

                        $("#startdate").on("change.datetimepicker", function (e) {
                            $('#enddate').datetimepicker('minDate', e.date);
                        });
                        $("#enddate").on("change.datetimepicker", function (e) {
                            $('#startdate').datetimepicker('maxDate', e.date);
                        });
                    });

                    function submitNewForm() {
                        var input = $("<input>")
                            .attr("type", "hidden")
                            .attr("name", "add_new").val("add_new");
                        $('#workform').append(input);
                        $("#workform").submit();
                    }
                </script>

        </div>
    </div>
@endsection
