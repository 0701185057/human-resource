
@extends('layouts.header')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet">
<hr>
<div class="container">
    <div class="row justify-content-center align-items-center m-4">
        <div class="col col-sm-8 align-self-center">
            <div class="card">
                <div class="card-body bg-primary">
                    <div class="text-center text-light">
                        Register
                    </div>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="/applicant_registration">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input class="form-control" type="text" name="first_name" id="first_name"
                                           placeholder="First Name" >
                                    @if ($errors->has('first_name'))
                                        <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                    @endif
                                </div><!--col-->
                            </div><!--row-->

                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>

                                    <input class="form-control" type="text" name="last_name" id="last_name"
                                           placeholder="Last Name">
                                    @if ($errors->has('last_name'))
                                        <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="email">E-mail Address</label>

                                    <input class="form-control" type="email" name="email" id="email" value
                                           placeholder="E-mail Address">
                                    @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input class="form-control" type="password" name="password" id="password"
                                           placeholder="Password" >
                                    @if ($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div><!--form-group-->
                            </div><!--col-->

                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="password_confirmation">Password Confirmation</label>

                                    <input class="form-control" type="password" name="password_confirmation"
                                           id="password_confirmation" placeholder="Password Confirmation" >
                                    @if ($errors->has('password_confirmation'))
                                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    <button class="btn btn-primary" type="submit">Register</button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                        <a class="btn btn-link" href="\applicant_login">
                            If already register please login here </a>
                    </form>
            </div><!-- card -->
        </div><!-- col-md-8 -->
    </div><!-- row -->
</div><!-- container -->
    </div><!--col-->
@endsection
