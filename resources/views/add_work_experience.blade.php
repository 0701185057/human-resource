@extends('application_form_header')

@section('content')
    <div class="container">
        {{ csrf_field() }}
        <div class="card">
            <div class="row justify-content-center align-items-center m-4">
                <div class="col col-sm-12 align-self-center">
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-info" role="alert">
                                Note: Add all your experiences.
                            </div>
                            <div>
                            </div>
                                <a href="/experience" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Add</a>
                            <br>
                            <br>

                            <hr>
                            <a href="/education" class="btn btn-outline-primary"><i class="fas fa-caret-left"></i> Previous </a>
                            <a href="/add_referee" class="btn btn-outline-primary">Next <i class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
