<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Latest compiled and minified JavaScript -->
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"
        rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<div class="container" id="box">
    <div class="card">
        <div class="row justify-content-center align-items-center m-4">
            <div class="col col-sm-12 align-self-center">
                <div class="row">
                    <div class="col">
                        <div>
                        </div>
                        <p class="font-weight-bold">PERSONAL PROFILE</p>
                        <hr>
                        @if($information != null && $information->profile != null)
                        FIRST NAME:{{$information->first_name}}
                        <br>
                        LAST NAME:{{$information->last_name}}
                        <br>
                        GENDER:{{$information->profile->gender}}
                        <br>
                        DOB:{{$information->profile->dob}}
                        <br>
                        PHONE NUMBER:{{$information->profile->phone}}
                        <br>
                        ALT PHONE NUMBER:{{$information->profile->alt_phone}}
                        <br>
                        POSTAL ADDRESS:{{$information->profile->postal_address}}
                        <br>
                        POSTAL CODE:{{$information->profile->postal_code}}
                        @endif
                        <hr>
                        <p class="font-weight-bold">EDUCATION BACKGROUND</p>
                        <hr>
                        @foreach($information->education as $e)
                            INSTITUTION:{{$e->institution}}
                            <br>
                            PROGRAM:{{$e->program}}
                            <br>
                            GRADE:{{$e->grade}}
                            <br>
                            STARTED AT:{{$e->startdate}}
                            <br>
                            END AT:{{$e->enddate}}
                            <br>
                            ATTACHMENT:{{$e->attachment}}
                            <br>
                            <hr>
                        @endforeach
                        <p class="font-weight-bold"> WORK EXPERIENCE:</p>
                        @foreach($information->experience as $e)
                            <hr>
                            EMPLOYER:{{$e->employer}}
                            <br>
                            JOB TITLE:{{$e->job_title}}
                            <br>
                            START DATE:{{$e->start_date}}
                            <br>
                            END DATE:{{$e->end_date}}
                            <br>
                            ACHIEVEMENTS:{{$e->start_date}}
                            <br>
                            AWARDS:{{$e->award}}
                            <br>
                        @endforeach
                        <hr>
                        <p class="font-weight-bold">REFEREE:</p>
                        <hr>
                        @foreach($information->referee as $e)
                            <br>
                            COMPANY:{{$e->company}}
                            <br>
                            NAME:{{$e->name}}
                            <br>
                            TITLE:{{$e->job_title}}
                            <br>
                            PHONE NUMBER:{{$e->phone}}
                            <br>
                            EMAIL:{{$e->email}}
                            <br>
                            POSTAL ADDRESS:{{$e->postal_address}}
                            <br>
                            POSTAL CODE:{{$e->postal_code}}
                            <hr>
                        @endforeach
                        <button class="btn btn-primary hidden-print" onclick="myFunction()"><span
                                class="glyphicon glyphicon-print" aria-hidden="true"></span> Print
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
window.print();
}
</script>
