
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
{{--    <script src="https://code.jquery.com/jquery-3.1.1.slim.js"></script>--}}
{{--    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>--}}

<!--Data Table-->
    <script type="text/javascript" src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

    <!--Export table button CSS-->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
<div id="app">
    <nav class="sticky-top navbar py-1 navbar-expand-lg navbar-light bg-gradient-white">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <div class="col-6 collapse-close">
                                <button type="button" class="navbar-toggler" data-toggle="collapse"
                                        data-target="#navbarSupportedContent"
                                        aria-controls="navbarSupportedContent" aria-expanded="false"
                                        aria-label="Toggle navigation">
                                </button>
                            </div>
                        </div>
                    </div>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="applicant_profile_details" class="font-weight-bold nav-link text-primary ">Home</a></li>
                        <li class="nav-item"><a href="" data-toggle="scroll" class="font-weight-bold nav-link text-primary">About us</a></li>
                        <li class="nav-item"><a href="#" data-toggle="scroll" class="font-weight-bold nav-link text-primary">Projects</a></li>
                        <li class="nav-item"><a href="#" data-toggle="scroll" class="font-weight-bold nav-link text-primary">our teams</a></li>
                        <li class="nav-item"><a href="#" data-toggle="scroll" class="font-weight-bold nav-link text-primary">services</a></li>
                        <li class="nav-item"><a href="" class="font-weight-bold nav-link text-primary">Career Openings</a></li>
                        <li class="nav-item dropdown">
                            <a href="#" class="font-weight-bold nav-link dropdown-toggle text-primary"
                               id="navbarDropdownMenuUser" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">{{Auth::guard('applicant')->user()->first_name}}  {{Auth::guard('applicant')->user()->last_name}}</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuUser">
                                <a href="/logout" class="nav-link ">Logout</a>
                                <a href="" class="nav-link ">profile</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
</body>
<hr>
<div class="main_nav">
    <div class="container">
        <div class="row justify-content-center align-items-center m-4">
            <div class="col col-sm-12 align-self-center">
                <div class="card">
                    <div class="card-header">
                        <strong>
                            My Account
                        </strong>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-pills">
                            <li class="nav-item ">
                                <a class="nav-link text-primary" href="applicant_profile_details">1. Profile</a>
                            </li>
                            <li class="nav-item text-primary">
                                <a class="nav-link" href="education">2. Education</a>
                            </li>
                            <li class="nav-item text-primary">
                                <a class="nav-link" href="add_work_experience">3. Work Experience</a>
                            </li>
                            <li class="nav-item text-primary">
                                <a class="nav-link" href="add_referee">4. Referees</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-primary" href="/summary">5. Summary</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    @if($errors->any())
        <div class="alert alert-danger">
            <p><strong>Opps Something went wrong</strong></p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">{{session('error')}}</div>
    @endif
<main class="py-4">
    @yield('content')
</main>
</div>
</html>

