@extends('application_form_header')

@section('content')
    <div class="container">
        <div class="card">
            <div class="row justify-content-center align-items-center m-4">
                <div class="col col-sm-12 align-self-center">
                    <form method="POST" action="/referee" id="refereeform" >
                        {{ csrf_field() }}
                        <div class="form-group col-sm-6">
                            <p>(Fields marked * are required)</p>
                        </div>
                        <!-- Company Field -->
                        <div class="form-group col-sm-6">
                            <label for="company">Company*:</label>
                            <input class="form-control" name="company" type="text" id="company">
                        </div>

                        <!-- Job title Field -->
                        <div class="form-group col-sm-6">
                            <label for="job_title">Job Title*:</label>
                            <input class="form-control" name="job_title" type="text" id="job_title">
                        </div>

                        <!-- Name Field -->
                        <div class="form-group col-sm-6">
                            <label for="name">Name*:</label>
                            <input class="form-control" name="name" type="text" id="name">
                        </div>

                        <!-- Phone Field -->
                        <div class="form-group col-sm-6">
                            <label for="phone">Phone* (include country code):</label>
                            <input class="form-control" name="phone" type="text" id="phone">
                        </div>

                        <!-- Email Field -->
                        <div class="form-group col-sm-6">
                            <label for="email">Email*:</label>
                            <input class="form-control" name="email" type="email" id="email">
                        </div>

                        <!-- Postal Address Field -->
                        <div class="form-group col-sm-6">
                            <label for="postal_address">Postal Address*:</label>
                            <input class="form-control" name="postal_address" type="text" id="postal_address">
                        </div>

                        <!-- Postal Code Field -->
                        <div class="form-group col-sm-6">
                            <label for="postal_code">Postal Code*:</label>
                            <input class="form-control" name="postal_code" type="text" id="postal_code">
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            <hr>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save & go back to
                                list
                            </button>
                            <button type="button" class="btn btn-secondary" onClick="submitNewForm()">Save & Add New
                                Entry
                            </button>
                            <a href="http://careers.jamii.co.ke/referees" class="btn btn-outline-warning">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
