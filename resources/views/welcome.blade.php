<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com -->
    <title>Tunner home page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            color: #818181;
        }
        h2 {
            font-size: 24px;
            text-transform: uppercase;
            color: #1E90FF;
            font-weight: 600;
            margin-bottom: 30px;
        }
        h4 {
            font-size: 19px;
            line-height: 1.375em;
            color: #303030;
            font-weight: 400;
            margin-bottom: 30px;
        }
        .jumbotron {
            background-color: #1E90FF;
            padding: 100px 25px;
            text-decoration-color:#1E90FF;
        }
        .container-fluid {
            padding: 60px 50px;
        }
        .bg-grey {
            background-color: #f6f6f6;
        }
        .logo-small {
            color: #1E90FF;
            font-size: 50px;
        }
        .logo {
            color: #1E90FF;
            font-size: 200px;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail img {
            width: 100%;
            height: 100%;
            margin-bottom: 10px;
        }
        .carousel-control.right, .carousel-control.left {
            background-image: none;
            color: #f4511e;

        }
        .carousel-indicators li {
            border-color: #f4511e;
        }
        .carousel-indicators li.active {
            background-color: #f4511e;
        }
        .item h4 {
            font-size: 19px;
            line-height: 1.375em;
            font-weight: 400;
            font-style: italic;
            margin: 70px 0;
        }
        .item span {
            font-style: normal;
        }
        .panel {
            border: 1px solid #f4511e;
            border-radius:0 !important;
            transition: box-shadow 0.5s;
        }
        .panel:hover {
            box-shadow: 5px 0px 40px rgba(0,0,0, .2);
        }
        .panel-footer .btn:hover {
            border: 1px solid #f4511e;
            background-color: #fff !important;
            color: #f4511e;
        }
        .panel-heading {
            color: #fff !important;
            background-color: #f4511e !important;
            padding: 25px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }
        .panel-footer {
            background-color: white !important;
        }
        .panel-footer h3 {
            font-size: 32px;
        }
        .panel-footer h4 {
            color: #aaa;
            font-size: 14px;
        }
        .panel-footer .btn {
            margin: 15px 0;
            background-color: #f4511e;
            color: #fff;
        }
        .navbar {
            margin-bottom: 0;
            z-index: 9999;
            border: 0;
            font-size: 16px !important;
            line-height: normal;
            border-radius: 0;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #1E90FF !important;
        }
        .navbar-nav li a:hover, .navbar-nav li.active a {
            color: #1E90FF;
            background-color: #fff !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
            color: #fff !important;
        }
        footer .glyphicon {
            font-size: 20px;
            margin-bottom: 20px;
            color: #1E90FF;
        }
        .slideanim {visibility:hidden;}
        .slide {
            animation-name: slide;
            -webkit-animation-name: slide;
            animation-duration: 1s;
            -webkit-animation-duration: 1s;
            visibility: visible;
        }
        @keyframes slide {
            0% {
                opacity: 0;
                transform: translateY(70%);
            }
            100% {
                opacity: 1;
                transform: translateY(0%);
            }
        }
        @-webkit-keyframes slide {
            0% {
                opacity: 0;
                -webkit-transform: translateY(70%);
            }
            100% {
                opacity: 1;
                -webkit-transform: translateY(0%);
            }
        }
        @media screen and (max-width: 768px) {
            .col-sm-4 {
                text-align: center;
                margin: 25px 0;
            }
            .btn-lg {
                width: 100%;
                margin-bottom: 35px;
            }
        }
        @media screen and (max-width: 480px) {
            .logo {
                font-size: 150px;
            }
        }
    </style>
    @extends('layouts.header')

    @section('content')
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="jumbotron text-center">
    <h1>Tunner company</h1>
    <p class="h3">We specialize in building and construction </p>
    <form>
        <div class="form-group row align-items-center">
            <input type="email" class="col-sm-10" placeholder="Email Address" required>
            <div class="input-group-btn">
                <button type="button" class="btn-col-sm-2 btn-danger">Subscribe</button>
            </div>
        </div>
    </form>
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid">
    <div class="row">
        <div class="col-sm-8">
            <h2>About Company </h2><br>
            <h4> Tunner Construction Limited has been involved in a wide variety of development projects and has successfully completed Civil Engineering and Environmental Services including: water supply and reticulation, dams, sewerage and drainage, building works, road construction, telecommunications and power works.
                We believe that our success hinges on the passion, expertise and dedication of our employees, and thus place utmost importance to employee welfare by creating a pleasant working environment.</h4>
        </div>
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-signal logo"></span>
        </div>
    </div>
</div>

<div class="container-fluid bg-grey">
    <div class="row">
        <div class="col-sm-4">
            <span class="glyphicon glyphicon-globe logo slideanim"></span>
        </div>
        <div class="col-sm-8">
            <h2>Our Values</h2><br>
            <h4><strong>MISSION:</strong> To provide excellent services to the people of Kenya, by offering professional expertise using state of the art tools and technology in computer hardware and software.
                                          The mission reflects our goals, ethics and attitudes towards our work. Innovation is key in dealing with the unique challenges of every project we undertake.</h4><br>
            <h4><strong>VISION:</strong>To be the most respected Construction Company in Kenya.
                                       We place great value on innovation and professionalism in everything we do. These qualities have proved paramount in our endeavour to be the Company of choice in infrastructure development in Kenya.</h4>
        </div>
    </div>
</div>
<!-- Container (Portfolio Section) project-->
<div id="portfolio" class="container-fluid text-center bg-grey">
    <h2>Portfolio</h2><br>
    <h4>What we have created</h4>
    <div class="row text-center slideanim">
        <div class="col-sm-4">
            <div class="thumbnail">
                <img src="paris.jpg" alt="" width="400" height="300">
                <p><strong></strong></p>
                <p></p>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="thumbnail">
                <img src="newyork.jpg" alt="" width="400" height="300">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="thumbnail">
                <img src="sanfran.jpg" alt="" width="400" height="300">
                <p class="h4"><strong></strong></p>
                <p class="h4"></p>
            </div>
        </div>
    </div><br>
    <!-- Container our team -->
    <div id="pricing" class="container-fluid">
        <div class="text-center">
            <h2>our team</h2>
        </div>
        <div class="row slideanim">
            <h4>Tunner Construction Limited has a highly qualified team of professional and technical staff.
                One of the directors is a professionally qualified civil engineer who possesses several years
                experience in design, construction, supervision and management of building and civil
                engineering projects, including infrastructure works and maintenance. One working director
                has remained active in engineering, offering his technical expertise and know-how to all the company’s
                operations. He is backed by a well motivated and qualified team of technical staff who ensure that
                projects are executed in an efficient, timely and professional manner.</h4>
        </div>
    </div>
        <!-- Container (Services Section) -->
        <div id="services" class="container-fluid text-center">
            <h2>SERVICES</h2>
            <h4>What we offer</h4>
            <br>
            <div class="row slideanim">
                <div class="col-sm-4">
                    <span class="glyphicon glyphicon-home logo-small"></span>
                    <h4>BUILDING WORK</h4>
                </div>
                <div class="col-sm-4">
                    <span class="glyphicon glyphicon-road logo-small"></span>
                    <h4>ROADS WOKR</h4>
                </div>
                <div class="col-sm-4">
                    <span class="glyphicon glyphicon-flash logo-small"></span>
                    <h4>POWER WORK</h4>
                </div>
            </div>
            <br><br>

        </div>
<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
    <h2 class="text-center">AVAILABLE JOBS</h2>
    <div class="container">
        <div class="row my-4">
        </div>
        <h3 class="text-center display-3 text-dark">Current Openings</h3>
        <div class="row m-4">
            <div class="col-md-12">
                <div class="card mb-2">
                    <div class="card-body">
                        <h4 class="display-4 card-title">
                            <a href="https://careers.jtl.co.ke/view/13f64473-4d65-430f-9871-dcb19553ca67" class="card-link">Direct Sales Agent (Mombasa Region)</a>
                        </h4>
                        <h4 class="card-subtitle my-2 text-muted">
                            JTL-SS-DSA(R-03-20
                        </h4>
                        <h4 class="card-subtitle my-2 text-muted">
                            Sales |
                            Contract
                        </h4>
                        <h4 class="card-subtitle mb-2 text-muted">
                            <i class="fas fa-calendar-check"></i>
                            Posted March 5th, 2020 |
                            <i class="fas fa-calendar-check"></i>
                            Expires March 31st, 2020 </h4>
                        <p class="card-text"><p><b>DIRECT SALES AGENT</b><br>We are looking to hire sales agents to promote our products and services, to identify customer needs and propose the best solutions that will achieve the set sales targets.<br><br><b>DIRECT SALES AGENTS JOB RESPONSIBILITIES</b><br>•    Source for new business ...</p>
                        <a href="https://careers.jtl.co.ke/view/13f64473-4d65-430f-9871-dcb19553ca67" class="btn btn-outline-primary"><i class="far fa-eye"></i> View</a>
                        <a href="https://careers.jtl.co.ke/export/13f64473-4d65-430f-9871-dcb19553ca67" class="btn btn-outline-secondary"><i class="far fa-file-pdf"></i> Download</a>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<!-- Image of location/map -->
<img src="/w3images/map.jpg" class="w3-image w3-greyscale-min" style="width:100%">

<footer class="container-fluid text-center">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
    <p class="h4"><a href="https://www.tunner.com" title="Visit www.tunner.com">www.tunner.com</a></p>
</footer>

<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>

</body>
</html>
@endsection
