@extends('modal')

@section('content')
    <h3>
        <div class="well"> Applicants Details</div>
    </h3>
    <div class="row-fluid">
        <table class="table" id="applicants">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email Address</th>
                <th scope="col">Phone Number</th>
                <th scope="col">Details</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            @foreach($information as $e)
                <tr>
                    <td>{{$e->id}}</td>
                    <td>{{$e->first_name}}</td>
                    <td>{{$e->last_name}}</td>
                    <td>{{$e->email}}</td>
                    @if($e != null && $e->phone != null)
                    <td>{{$e->profile->phone}}</td>
                    @endif
                    <td><a class="btn btn-primary" href="/curriculumvitae/{{$e->id}}" role="button">Details</a></td>
                    <td><a class="btn btn-primary" href="#" role="button">Delete</a></td>
                </tr>
                @endforeach
                </body>
        </table>
    </div>
    <body>
@endsection
{{--                <script>--}}

{{--                    $(document).ready(function () {--}}

{{--                        var table = $('#applicants').DataTable({--}}
{{--                            "processing": true,--}}
{{--                            "serverSide": true,--}}
{{--                            type: 'get',--}}
{{--                            "ajax": '{{url('/hr_applicant_display')}}',--}}

{{--                            "columns": [--}}
{{--                                {"data": "id"},--}}
{{--                                {"data": "first_name"},--}}
{{--                                {"data": "last_name"},--}}
{{--                                {"data": "email"},--}}
{{--                                {"data": "phone_number"},--}}
{{--                                {"data": "details"},--}}
{{--                                {"data": "delete"}--}}
{{--                            ], dom: 'Bfrtip',--}}
{{--                            buttons: [--}}
{{--                                'copy', 'csv', 'excel', 'pdf', 'print'--}}
{{--                            ]--}}

{{--                        });--}}
{{--                    });--}}
{{--                </script>--}}


