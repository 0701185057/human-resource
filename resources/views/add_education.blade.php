@extends('application_form_header')

@section('content')
    <div class="container">
        <div class="card">
            <div class="row justify-content-center align-items-center m-4">
                <div class="col col-sm-12 align-self-center">
                    <div class="row">
                    </div>
                    <form method="POST" action="/add_education" id="educationform" enctype="multipart/form-data">
                        <div class=" form-group col-sm-6">
                    <p>(Fields marked * are required)</p>
                </div>
            {{ csrf_field() }}
                <!-- Institution Field -->
                <div class="form-group col-sm-6">
                    <label for="institution">Institution*:</label>
                    <select class="form-control"  id="institution"
                            name="institution">
                        <option selected="selected" value="">Please select</option>
                        <option value="1">Adept College of Professional Studies</option>
                        <option value="2">Adventist University of Africa</option>
                        <option value="3">Africa College of Social Work</option>
                        <option value="4">Africa Digital Media Institute</option>
                        <option value="5">Africa International University</option>
                        <option value="6">Africa Nazarene University</option>
                        <option value="7">African Institute of Research and Development Studies</option>
                        <option value="8">Aga Khan University Teaching Hospital</option>
                        <option value="9">AirSwiss International College</option>
                        <option value="10">AirSwiss International College</option>
                        <option value="11">Airways Travel Institute</option>
                        <option value="12">Alphax College</option>
                        <option value="13">Amani College</option>
                        <option value="14">Amboseli Institute of Hospitality and Technology</option>
                        <option value="15">Amref International University (AMIU)</option>
                        <option value="16">Arkline College</option>
                        <option value="17">Associated Computer Services</option>
                        <option value="18">Atlas College</option>
                        <option value="19">AUGAB Computer College</option>
                        <option value="20">Augustana College</option>
                        <option value="21">Australian Studies Institute (AUSI)</option>
                        <option value="22">Baraton Teachers&#039; Training College</option>
                        <option value="23">Bell Institute of Technology</option>
                        <option value="24">Belmont International College</option>
                        <option value="25">Bible College of East Africa</option>
                        <option value="26">BizSmart Inter Technology</option>
                        <option value="27">Bungoma Technical Training Institute</option>
                        <option value="28">Career Training Centre</option>
                        <option value="29">Cascade Institute of Hospitality</option>
                        <option value="30">Catholic Higher Institute of Eastern Africa</option>
                        <option value="31">Catholic University of Eastern Africa(CUEA)</option>
                        <option value="32">Centre for Distance &amp; Online Learning</option>
                        <option value="33">Century Park College</option>
                        <option value="34">Chuka University</option>
                        <option value="35">Coast Institute of Technology</option>
                        <option value="36">College of Management Sciences</option>
                        <option value="37">Compuera College</option>
                        <option value="38">Compugoal College</option>
                        <option value="39">Computer Learning Centre (CLC)</option>
                        <option value="40">Computer Pride Training Centre</option>
                        <option value="41">Computer Training Centre</option>
                        <option value="42">Consolata Institute of Communication and Technology</option>
                        <option value="43">Cornerstone Training Institute</option>
                        <option value="44">Daystar University</option>
                        <option value="45">Dedan Kimathi University of Technology</option>
                        <option value="46">Digital Resource Center(DRC)</option>
                        <option value="47">Digiworld Computer School</option>
                        <option value="48">Don Bosco Boy&#039;s Town</option>
                        <option value="49">Don Bosco Institute of Management Studies</option>
                        <option value="50">Duolotech Computers</option>
                        <option value="51">Eagle Air Aviation College (EAAC)</option>
                        <option value="52">Eagle College of Management Studies</option>
                        <option value="53">East Africa Institute of Certified Studies</option>
                        <option value="54">East Africa Institute of Certified Studies</option>
                        <option value="55">East Africa School of Journalism (EASJ)</option>
                        <option value="56">East Africa School of Management</option>
                        <option value="57">East Africa Vision Institute</option>
                        <option value="58">East African Media Institute (EAMI)</option>
                        <option value="59">East African School of Aviation</option>
                        <option value="60">Egerton University</option>
                        <option value="61">Eldoret Aviation Training Institute</option>
                        <option value="62">Eldoret Polytechnic</option>
                        <option value="63">Elite Centre</option>
                        <option value="64">Elite Commercial Institute</option>
                        <option value="65">Elix Centre of Informatics</option>
                        <option value="66">Emanex Computer College</option>
                        <option value="67">Emma Daniel Arts Training Institute (EDATI)</option>
                        <option value="68">Esmart College</option>
                        <option value="69">Felma College</option>
                        <option value="70">German Institute of Professional Studies</option>
                        <option value="71">Globoville Shanzu Beach College</option>
                        <option value="72">Government Training Institute (GTI)</option>
                        <option value="73">Graffins College</option>
                        <option value="74">Great Lakes University of Kisumu</option>
                        <option value="75">Gretsa University</option>
                        <option value="76">Gusii Institute of Technology</option>
                        <option value="77">Gusii Institute of Technology</option>
                        <option value="78">Hansons College of Professional Studies</option>
                        <option value="79">Harvard Institute of Development Studies</option>
                        <option value="80">Hemland College of Professional and Technical Studies</option>
                        <option value="81">Hemland Computer Institute Thika</option>
                        <option value="82">Higher Institute of Development Studies</option>
                        <option value="83">Hi-tec Institute of Professional Studies</option>
                        <option value="84">Holy Rosary College</option>
                        <option value="85">ICT Fire and Rescue</option>
                        <option value="86">Indian Institute of Hardware Technology</option>
                        <option value="87">Institute of Advanced Technology</option>
                        <option value="88">Institute of Advanced Technology Campus</option>
                        <option value="89">Institute of Business and Technology</option>
                        <option value="90">Institute of Information Technology Studies &amp; Research</option>
                        <option value="91">Institute of Zaburi Technologies</option>
                        <option value="92">Inter-Afrika Development Institute</option>
                        <option value="93">International Centre of Technology (ICT)</option>
                        <option value="94">International College of Kenya</option>
                        <option value="95">International Hotel &amp; Tourism Institute</option>
                        <option value="96">InterWorld College</option>
                        <option value="97">Intraglobal Training Institute</option>
                        <option value="98">Jaffery Institute of Professional Studies</option>
                        <option value="99">Jodan College of Technology</option>
                        <option value="100">Jogoo Commercial College</option>
                        <option value="101">Jomo Kenyatta University of Agriculture and Technology</option>
                        <option value="102">K.A.G. East University</option>
                        <option value="103">Kabarak University</option>
                        <option value="104">Kabete National Polytechnic</option>
                        <option value="105">Kagumo College</option>
                        <option value="106">Kaiboi Technical Training Institute</option>
                        <option value="107">Karatina Institute of Technology(KIT</option>
                        <option value="108">Karatina University</option>
                        <option value="109">KCA University</option>
                        <option value="110">Keiway Mining &amp; Technology College</option>
                        <option value="111">Kenair travel and related studies</option>
                        <option value="112">Kenya Aeronautical College</option>
                        <option value="113">Kenya Christian Industrial Training Institute (KCITI)</option>
                        <option value="114">Kenya College of Communications Technology</option>
                        <option value="115">Kenya College of Medicine &amp; Related Studies</option>
                        <option value="116">Kenya College of Skills and Talent Development</option>
                        <option value="117">Kenya Forestry College</option>
                        <option value="118">Kenya Forestry Research Institute</option>
                        <option value="119">Kenya Institute of Administration (KIA)</option>
                        <option value="120">Kenya Institute of Applied Sciences</option>
                        <option value="121">Kenya Institute of Biomedical Sciences and Technology (KIBSAT)</option>
                        <option value="122">Kenya Institute of Development Studies (KIDS)</option>
                        <option value="123">Kenya Institute of Highways and Building Technology (KIHBT)</option>
                        <option value="124">Kenya Institute of Management (KIM)</option>
                        <option value="125">Kenya Institute of Mass Communication</option>
                        <option value="126">Kenya Institute of Media and Technology(KIMT)</option>
                        <option value="127">Kenya Institute of Monitoring and Evaluation Studies (KIMES)</option>
                        <option value="128">Kenya Institute of Professional Studies</option>
                        <option value="129">Kenya Institute of Social Work and Community Development(KISWCD)
                        </option>
                        <option value="130">Kenya Institute of Software Engineering</option>
                        <option value="131">Kenya Institute of Special Education (KISE)</option>
                        <option value="132">Kenya Medical Training Centre (KMTC)</option>
                        <option value="133">Kenya Methodist University</option>
                        <option value="134">Kenya School of Accountancy and Finance</option>
                        <option value="135">Kenya School of Government</option>
                        <option value="136">Kenya School of Medical Science and Technology</option>
                        <option value="137">Kenya School of Monetary Studies</option>
                        <option value="138">Kenya School of Professional Counseling &amp; Behavioural Sciences
                            (KSCBS)
                        </option>
                        <option value="139">Kenya School of Professional Studies (KSPS)</option>
                        <option value="140">Kenya School of Professional Studies (SPS)</option>
                        <option value="141">Kenya School of Technology Studies (KSTS)</option>
                        <option value="142">Kenya Science Teachers College</option>
                        <option value="143">Kenya Technical Teachers College (KTTC)</option>
                        <option value="144">Kenya Utalii College</option>
                        <option value="145">Kenya Water Institute</option>
                        <option value="146">Kenya Wildlife Service Training Institute</option>
                        <option value="147">Kenyaplex Institute of TechnologyMwala</option>
                        <option value="148">Kenyatta University</option>
                        <option value="149">Kericho Teachers College</option>
                        <option value="150">Kenya School of Government (Formerly Kenya Institute of Administration
                            (KIA))
                        </option>
                        <option value="151">Kiambu Institute of Science and Technology</option>
                        <option value="152">Kigari Teachers College</option>
                        <option value="153">Kilimambogo Teachers College</option>
                        <option value="154">Kima International School of Theology (KIST)</option>
                        <option value="155">Kimathi Chambers</option>
                        <option value="156">Kinyanjui Technical Training Institute</option>
                        <option value="157">Kirinyaga University</option>
                        <option value="158">Kiriri Women&#039;s University of Science and Technology</option>
                        <option value="159">Kisumu Polytechnic</option>
                        <option value="160">Kitale Technical Institute</option>
                        <option value="161">Laikipia University</option>
                        <option value="162">Lake Region Business training and Consultancy</option>
                        <option value="163">Lakeview Training Institute</option>
                        <option value="164">Lukenya University</option>
                        <option value="165">Machakos Institute of Technology</option>
                        <option value="166">Machakos University</option>
                        <option value="167">Management University of Africa</option>
                        <option value="168">Mark University of IT</option>
                        <option value="169">Maseno University</option>
                        <option value="170">Masinde Muliro University of Science and Technology</option>
                        <option value="171">Mawego Technical Institute</option>
                        <option value="172">Maxton College Of Media &amp; Communications</option>
                        <option value="173">Mboya Labour College</option>
                        <option value="174">Meru Technical Institute</option>
                        <option value="175">Meru University of Science and Technology</option>
                        <option value="176">Michuki Technical Institute</option>
                        <option value="177">Migori Teachers college</option>
                        <option value="178">Moi Institute of Technology</option>
                        <option value="179">Moi University</option>
                        <option value="180">Mosoriot Teachers College</option>
                        <option value="181">Motion City International</option>
                        <option value="182">Multimedia University of Kenya</option>
                        <option value="183">Muranga Institute of Technology</option>
                        <option value="184">Muranga University of Technology</option>
                        <option value="185">Na rap Training Institute</option>
                        <option value="186">Nairobi Aviation College</option>
                        <option value="187">Nairobi Film School</option>
                        <option value="188">Nairobi Institute of Business studies (NIBS)</option>
                        <option value="189">Nairobi Institute of Software Development</option>
                        <option value="190">Nairobi Institute of Technology</option>
                        <option value="191">Nairobi International School of Theology</option>
                        <option value="192">Naivasha Computer &amp; Business Studies College</option>
                        <option value="193">Nakuru College of Health Sciences and Management</option>
                        <option value="194">Nakuru Counseling &amp; Training Institute</option>
                        <option value="195">Nakuru Institute of Information Communication Technology</option>
                        <option value="196">Narok Teachers College</option>
                        <option value="197">Narok Teachers Training</option>
                        <option value="198">National Youth Service Engineering Institute</option>
                        <option value="199">Nationwide Hotel and Tourism College (NHTC)</option>
                        <option value="200">Neema Lutheran College</option>
                        <option value="201">NEWVIEW COLLEGE</option>
                        <option value="202">Nkabune Technical Institute</option>
                        <option value="203">Oshwal College</option>
                        <option value="204">Pan Africa Christian University</option>
                        <option value="205">Pan African School of Theology (PAST)</option>
                        <option value="206">PC Kinyanjui Technical Training Institute (PCKTTI)</option>
                        <option value="207">PCEA Shalom Training College</option>
                        <option value="208">Pioneers Training Institute</option>
                        <option value="209">PREMESE Africa Development Institute</option>
                        <option value="210">Premier College of Hospitality and Business Studies</option>
                        <option value="211">Premier College of Professional Studies Ltd</option>
                        <option value="212">Presbyterian University of East Africa</option>
                        <option value="213">Prestige Academy and College</option>
                        <option value="214">Railway Training Institute</option>
                        <option value="215">Ramogi Institute of Advanced Technology</option>
                        <option value="216">Regional Centre For Tourism And Foreign language</option>
                        <option value="217">Regional Training Institute</option>
                        <option value="218">Regions Group International College</option>
                        <option value="219">Rehoboth College</option>
                        <option value="220">Riara University</option>
                        <option value="221">Riccatti Business College of East Africa</option>
                        <option value="222">Rift valley institute of Business studies Nakuru and kericho</option>
                        <option value="223">Rift Valley Institute Of Science &amp; Technology</option>
                        <option value="224">Rift Valley Technical Training Institute</option>
                        <option value="225">Rochester Business School</option>
                        <option value="226">Royal Institute of Applied Sciences</option>
                        <option value="227">Sacred Lake Institute of Technology</option>
                        <option value="228">Sacred Training Institute</option>
                        <option value="229">Sagana Institute of Technology</option>
                        <option value="230">Savannah Institute for Business and Informatics</option>
                        <option value="231">School of ICT &amp; Hairdressing and Beauty</option>
                        <option value="232">School of Professional Studies</option>
                        <option value="233">Scott Christian University</option>
                        <option value="234">Sensei Institute of Technology for Plant Operator Training</option>
                        <option value="235">Shalom Information Technology Center</option>
                        <option value="236">Shanzu Teachers College</option>
                        <option value="237">Shepherds Foundation Education &amp; Research Centre</option>
                        <option value="238">Sirisia Youth Polytechnic</option>
                        <option value="239">Skynet Business College</option>
                        <option value="240">Skypath Aviation College</option>
                        <option value="241">SMA Swiss Management Academy</option>
                        <option value="242">Softpro Computer Institute</option>
                        <option value="243">South Eastern Kenya University</option>
                        <option value="244">South Rift International College (SORICO)</option>
                        <option value="245">St Joseph&#039;s Medical training College</option>
                        <option value="246">St. Andrew&#039;s Pre-Medical College</option>
                        <option value="247">St. Joseph Vocational Training Centre</option>
                        <option value="248">St. Mary&#039;s School of Clinical Medicine</option>
                        <option value="249">St. Paul&#039;s University</option>
                        <option value="250">Stanbridge College</option>
                        <option value="251">Star Media Institute</option>
                        <option value="252">Starnet College</option>
                        <option value="253">Stonebic College</option>
                        <option value="254">Strathmore University</option>
                        <option value="255">Superior Group of Colleges Intl.</option>
                        <option value="256">Talent institute</option>
                        <option value="257">Tambach Teachers Training College</option>
                        <option value="258">Tangaza College</option>
                        <option value="259">Taznaam Tutorial College</option>
                        <option value="260">Teachers College</option>
                        <option value="261">Tec Institute of Management</option>
                        <option value="262">Technical Training Institute (MTTI)</option>
                        <option value="263">Technical University of Mombasa</option>
                        <option value="264">Techno Links Ltd</option>
                        <option value="265">The East Africa School of Theology</option>
                        <option value="266">The East African University</option>
                        <option value="267">The iNet College</option>
                        <option value="268">The Kenya College of Science and Technology</option>
                        <option value="269">The Regional Institute of Business Management</option>
                        <option value="270">Thomas Asingo College of Computer and Business Management</option>
                        <option value="271">Times Training Centre</option>
                        <option value="272">United Africa College</option>
                        <option value="273">Universal Group of Colleges</option>
                        <option value="274">University of Eastern Africa</option>
                        <option value="275">University of Eldoret</option>
                        <option value="276">University of Kabianga</option>
                        <option value="277">University of Nairobi</option>
                        <option value="278">Uzima University College(constituent college of CUEA)</option>
                        <option value="279">Valley Institute of Science &amp; Technology</option>
                        <option value="280">Vision Empowerment Training Institute</option>
                        <option value="281">Vision Institute of Professionals</option>
                        <option value="282">Vision Stars Training Institute</option>
                        <option value="283">Wang Point Technologies College of Information Technology</option>
                        <option value="284">Western College of Hospitality and Professional Studies</option>
                        <option value="285">Zetech College</option>
                        <option value="286">Zetech University</option>
                        <option value="287">Other</option>
                    </select>
                </div>
                <!-- Program Field -->
                <div class="form-group col-sm-6">
                    <label for="program">Course*:</label>
                    <input class="form-control" name="program" type="text" id="program">
                </div>
                <!-- Field of study Field -->
                <div class="form-group col-sm-6">
                    <label for="field_of_study">Field of Study*:</label>
                    <input class="form-control" name="field_of_study" type="text" id="field_of_study">
                </div>
                <!-- Level Field -->
                <div class="form-group col-sm-6">
                    <label for="level">Education Level*:</label>
                    <select class="form-control" id="level" name="level">
                        <option selected="selected" value="">Please select</option>
                        <option value="KSCE">KSCE Certificate</option>
                        <option value="Certificate">Certificate</option>
                        <option value="Diploma">Diploma</option>
                        <option value="Undergraduate">Undergraduate</option>
                        <option value="Higher National Diploma">Higher National Diploma</option>
                        <option value="Masters">Masters</option>
                        <option value="Doctoral">Doctoral</option>
                    </select>
                </div>

                <!-- Grade Field -->
                <div class="form-group col-sm-6">
                    <label for="grade">Grade*:</label>
                    <input class="form-control" name="grade" type="text" id="grade">
                </div>

                <!-- Start Date Field -->
                <div class="form-group col-sm-6">
                    <label for="start_date">Start Date*:</label>
                    <input class="form-control" id="startdate" data-toggle="datetimepicker"
                           data-target="#startdate" name="startdate" type="date">
                </div>

                <!-- End Date Field -->
                <div class="form-group col-sm-6">
                    <label for="end_date">End Date: (Leave empty if in progress) </label>
                    <input class="form-control" id="enddate" data-toggle="datetimepicker"
                           data-target="#enddate" name="enddate" type="date">
                </div>

                <!-- Name Field -->
                <div class="form-group col-sm-6">
                    <label for="attachment">Attachment: (Scanned copies of certifications)</label>
                    <input name="attachment" type="file" id="attachment">
                </div>

                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    <hr>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save & go back to list</button>
                    <button type="button" class="btn btn-secondary" onClick="submitNewForm()">Save & Add New Entry</button>
                    <a href="/education" class="btn btn-outline-warning">Go back</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#startdate').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false,
                maxDate: moment()
            });

            $('#enddate').datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false,
                maxDate: moment()
            });

            $("#startdate").on("change.datetimepicker", function (e) {
                $('#enddate').datetimepicker('minDate', e.date);
            });
            $("#enddate").on("change.datetimepicker", function (e) {
                $('#startdate').datetimepicker('maxDate', e.date);
            });
        });

        function submitNewForm() {
            var input = $("<input>")
                .attr("type", "hidden")
                .attr("name", "add_new").val("add_new");
            $('#educationform').append(input);
            $("#educationform").submit();
        }
    </script>

    </div>
            </div>
        </div>
    </div>

@endsection

