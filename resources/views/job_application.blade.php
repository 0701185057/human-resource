<!-- Latest compiled and minified JavaScript -->
<!-- Fonts -->


<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/navigation.css') }}" rel="stylesheet">
<div class = "main">
<h3>
    <div class="well">JOB APPLICATION FORM</div>
</h3>
    <form  action="/job_application" method="post" enctype="multipart/form-data">
        @csrf
        <div class="well">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">Full Name:</label>
            <input type="name col-sm-4" name="name" class="form-control" placeholder="Enter your full name" id="name">
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            <label for="email">Email address:</label>
            <input type="email" name="email" class="form-control" placeholder="Enter email" id="email">
            <span class="text-danger">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
            <label for="phone">Phone:</label>
            <input type="phone" name="phone" class="form-control" placeholder="Enter phone number" id="phone">
            <span class="text-danger">{{ $errors->first('phone') }}</span>
        </div>
        <div class="form-group {{ $errors->has('experience') ? 'has-error' : '' }}">
            <label for="experience">Experience:</label>
            <div class="select">
                <select class="form-control form-control-lg" name="experience" size="3" multiple>
                    <option value="0 - 4 years">0 - 4 years</option>
                    <option value="5 - 10 years">5 - 10 years</option>
                    <option value="over 10 years">over 10</option>
                </select>
                <span class="text-danger">{{ $errors->first('experience') }}</span>
            </div>
        </div>
        <div class="form-group {{ $errors->has('cv') ? 'has-error' : '' }}">
            <label for="cv">Upload your cv:</label>
            <div class="custom-file">
                <input type="file" name="cv" class="custom-file-input" id="cv">
                <label class="custom-file-label" for="customFile">Choose file</label>
                <span class="text-danger">{{ $errors->first('cv') }}</span>
            </div>
        </div>
        <div class="form-group {{ $errors->has('cv') ? 'has-error' : '' }}">
            <label for="cv">Upload your passport:</label>
            <div class="custom-file">
                <input type="file" name="passport" class="custom-file-input" id="passport">
                <label class="custom-file-label" for="customFile">Choose file</label>
                <span class="text-danger">{{ $errors->first('passport') }}</span>
            </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </div>
    </form>
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".input-group input-file").on("change", function () {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".btn btn-default btn-choose").addClass("selected").html(fileName);
        });
    </script>

