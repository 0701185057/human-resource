@extends('layouts.header')

@section('content')
    <!--Data Table-->
    <script type="text/javascript" src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
            src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

    <!--Export table button CSS-->
    <link href="{{ asset('css/navigation.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <h3>
        <div class="well"> Applicants Details</div>
    </h3>
    <div class="row-fluid">
        <table class="table" id="applicants">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Full Name</th>
                <th scope="col">Email Address</th>
                <th scope="col">Phone</th>
                <th scope="col">Experience</th>
                <th scope="col">passport</th>
                <th scope="col">Curriculum vitae file</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <body>
            <script>

                $(document).ready(function () {

                    var table = $('#applicants').DataTable({
                            "processing": true,
                            "serverSide": true,
                             type: 'get',
                            "ajax": '{{url('/applicants_display')}}',
                            "columns": [
                                {data: 'id', id: 'id'},
                                {data: 'name', id: 'name'},
                                {data: 'email', id: 'email'},
                                {data: 'phone', id: 'phone'},
                                {data: 'experience', id: 'experience'},
                                {data: 'passport', id: 'passport'},
                                {data: 'download', id: 'download'},
                                {data: 'delete', id: 'delete'}
                            ], dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]

                        });
                });
            </script>

            </body>
        </table>
    </div>
@endsection
