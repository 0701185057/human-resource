@extends('application_form_header')

@section('content')
    <div class="container">
        <div class="card">
        <div class="row justify-content-center align-items-center m-4">
            <div class="col col-sm-12 align-self-center">
<form class="form-horizontal" method="POST" action="/applicant_profile_details" >
    {{ csrf_field() }}
    <p>(Fields marked * are required)</p>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="disabledTextInput">E-mail</label>
                        <input type="text" id="email" name="email" class="form-control" value="{{Auth::guard('applicant')->user()->email}}"  disabled>
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="title">Title*</label>:

                        <select class="form-control" name="title" id="title" maxlength="191" ><option value selected="selected">Please select</option><option value="mr">Mr.</option><option value="mrs">Mrs.</option><option value="other">Other</option></select>
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="first_name">First Name*</label>:
                        <input class="form-control" type="text" name="first_name" id="first_name" value="{{Auth::guard('applicant')->user()->first_name}}" placeholder="First Name" maxlength="191"  autofocus>
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="last_name">Last Name*</label>:

                        <input class="form-control" type="text" name="last_name" id="last_name" value="{{Auth::guard('applicant')->user()->last_name}}" placeholder="last_name" maxlength="191" >
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="gender">Gender*</label>:

                        <select class="form-control" name="gender" id="gender" maxlength="191" ><option value selected="selected">Please select</option><option value="male">Male</option><option value="female">Female</option><option value="other">Other</option></select>
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="dob">Date of Birth*</label>:

                        <input class="form-control" type="date" name="dob" id="datepicker" value data-toggle="datetimepicker" data-target="#datepicker" >
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="phone">Phone*</label>:

                        <input class="form-control" type="text" name="phone_number" id="phone_number" value placeholder="254XXXXXXXXX" maxlength="14" >
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="alt_phone">Alternative Phone</label>:

                        <input class="form-control" type="text" name="alt_phone" id="alt_phone" value placeholder="254XXXXXXXXX" maxlength="14">
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="postal_address">Postal Address*</label>:

                        <input class="form-control" type="text" name="postal_address" id="postal_address" value placeholder="P.O.Box" maxlength="100">
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col">
                    <div class="form-group col-sm-12">
                        <label for="postal_code">Postal Code*</label>:

                        <input class="form-control" type="text" name="postal_code" id="postal_code" value placeholder="00100" maxlength="12">
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div>
    </div>


    <div class="row">
        <div class="col">
            <div class="form-group col-sm-12 mb-0 clearfix">
                <hr>
                <button class="btn btn-primary pull-right" type="submit"><i class="fas fa-save"></i> Save</button>
                <a href="education" class="btn btn-outline-primary">Next <i class="fas fa-caret-right"></i></a>

            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
</form>
</div><!--card body-->
</div><!-- card -->
</div><!-- col-xs-12 -->
</div><!-- row -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('#datepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
        });
    });
</script>
@endsection
