<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Latest compiled and minified JavaScript -->
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"
        rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navigation.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="sticky-top navbar py-1 navbar-expand-lg navbar-light bg-gradient-white">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <div class="col-6 collapse-close">
                                <button type="button" class="navbar-toggler" data-toggle="collapse"
                                        data-target="#navbarSupportedContent"
                                        aria-controls="navbarSupportedContent" aria-expanded="false"
                                        aria-label="Toggle navigation">
                                </button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
                    <ul class="navbar-nav">
                        <li class="nav-item"><a href="" class="font-weight-bold nav-link text-primary ">Home</a></li>
                        <li class="nav-item"><a href="" data-toggle="scroll"
                                                class="font-weight-bold nav-link text-primary">About us</a></li>
                        <li class="nav-item"><a href="#" data-toggle="scroll"
                                                class="font-weight-bold nav-link text-primary">Projects</a></li>
                        <li class="nav-item"><a href="#" data-toggle="scroll"
                                                class="font-weight-bold nav-link text-primary">our teams</a></li>
                        <li class="nav-item"><a href="#" data-toggle="scroll"
                                                class="font-weight-bold nav-link text-primary">services</a></li>
                        <li class="nav-item"><a href="" class="font-weight-bold nav-link text-primary">Career Openings</a></li>
                        <li class="nav-item dropdown">
                            <a href="#" class="font-weight-bold nav-link dropdown-toggle text-primary"
                               id="navbarDropdownMenuUser" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">Account</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuUser">
                                <a href="" class="nav-link ">Login</a>
                                <a href="" class="nav-link ">Register</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="main_nav">
        <div class="container">
            <div class="row justify-content-center align-items-center m-4">
                <div class="col col-sm-12 align-self-center">
                    <div class="card">
                        <div class="card-header">
                            <strong>
                                My Account
                            </strong>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-pills">
                                <li class="nav-item ">
                                    <a class="nav-link" href="applicant_profile_details">1. Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="education">2. Education</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="add_work_experience">3. Work Experience</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="add_referee">4. Referees</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="summary">5. Summary</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
<main class="py-4">
    @yield('content')
</main>
</div>
</body>
</html>
