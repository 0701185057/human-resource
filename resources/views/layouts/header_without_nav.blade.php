@extends('layouts.header')

@section('content')
    <div class="main">
        <div class="container">
            @auth
            <div class="container-fluid">
                <div class="row right">
                    <div class="col-md-10">
                    </div>
                    <div class="col-md-2 right divdrop">
                        <div class="dropdown right">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                            </button>
                            <div class="dropdown-content">
                                <a href="/profile">Profile</a>
                                <a href="add_employee">Add </a>
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button class="btn btn-danger navbar-btn">Logout</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div>
            @endauth
            @yield('content')
        </div>
    </div>
    </div>
@endsection

