@extends('modal')

@section('content')
    <div class="container">
        <div class="row justify-content-left align-items-left m-0">
            <div class="col col-sm-6 align-self-left">
                <div class="card">
                    <div class="card-header">
                        <strong>
                            My profile details
                        </strong>
                    </div>
                    <br>
                    <div class="well col-sm-6">
                        <form>
                            @csrf
                            <div class="col-xs-6">
                                <input type="name" value="{{Auth::user()->first_name}}" name="first_name"
                                       class="form-control"
                                       id="first_name">
                            </div>
                            <br>
                            <div class="col-xs-6">
                                <input type="name" value="{{Auth::user()->middle_name}}" name="middle_name"
                                       class="form-control"
                                       id="middle_name">
                            </div>
                            <br>
                            <div class="col-xs-6">
                                <input type="name" value="{{Auth::user()->last_name}}" name="last_name" name="last_name"
                                       class="form-control" id="last_name">
                            </div>
                            <br>
                            <div class="col-xs-6">
                                <input type="email" value="{{Auth::user()->email}}" name="email" class="form-control"
                                       id="email">
                            </div>
                            <br>
                            <div class="col-xs-6">
                                <input type="name" class="form-control" value="{{Auth::user()->phone_number}}"
                                       id="phone_number"
                                       name="phone_number">
                            </div>
                            <br>
                            <div class="col-xs-6">
                                <input type="name" class="form-control" value="{{Auth::user()->id_number}}"
                                       id="id_number"
                                       name="id_number">
                            </div>
                            <br>
                            <div class="col-xs-6">
                                <input type="name" name="staff_number" class="form-control"
                                       value="{{Auth::user()->staff_number}}"
                                       id="staff_number">
                            </div>
                            <br>
                            <button type="submit" id="submit" class="btn btn-primary">Update</button>
                        </form>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });
            $("#submit").click(function (e) {
                e.preventDefault();
                var first_name = $("input[id=first_name]").val();
                var middle_name = $("input[id=middle_name]").val();
                var last_name = $("input[id=last_name]").val();
                var email = $("input[id=email]").val();
                var phone_number = $("input[id=phone_number]").val();
                var id_number = $("input[id=id_number]").val();
                var staff_number = $("input[id=staff_number]").val();
                $.ajax({
                    type: 'POST',
                    url: '/update_profile',
                    data: {
                        first_name: first_name,
                        middle_name: middle_name,
                        last_name: last_name,
                        email: email,
                        phone_number: phone_number,
                        id_number: id_number,
                        staff_number: staff_number
                    },
                    success: function (data) {
                        alert('updated successfully');
                        $('#submit').html('update')
                    }
                    , error: function (data) {
                        alert("Error try again");
                        $('#submit').html('RE-SEND')
                    }
                });
            });
        });
    </script>
@endsection

