@extends('layouts.header')

@section('content')
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"
        rel="stylesheet">
    <hr>
    <div class="container">
        <div class="row justify-content-center align-items-center m-4">
            <div class="col col-sm-8 align-self-center">
                <div class="card">
                    <div class="card-body bg-primary">
                        <div class="text-center text-light">
                            Login
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center align-items-center m-4">
                <div class="col col-sm-8 align-self-center">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="/login_applicant">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="email">E-mail Address</label>

                                            <input class="form-control" type="email" name="email" id="email" value
                                                   placeholder="E-mail Address" maxlength="191" >
                                        </div><!--form-group-->
                                    </div><!--col-->
                                </div><!--row-->

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="password">Password</label>

                                            <input class="form-control" type="password" name="password" id="password"
                                                   placeholder="Password" >
                                        </div><!--form-group-->
                                    </div><!--col-->
                                </div><!--row-->

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label for="remember"><input type="checkbox" name="remember"
                                                                             id="remember"
                                                                             value="1" checked> Remember Me</label>
                                            </div>
                                        </div><!--form-group-->
                                    </div><!--col-->
                                </div><!--row-->

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group clearfix">
                                            <button class="btn btn-primary" type="submit">Login</button>

                                            <a href="" class="float-right">Forgot Your Password?</a>
                                        </div><!--form-group-->
                                    </div><!--col-->
                                </div><!--row-->
                                <a class="btn btn-link" href="\applicant_registration">
                                    If not in the system please register here? </a>
                            </form>

                            <div class="row">
                                <div class="col">
                                </div><!--col-->
                            </div><!--row-->
                        </div><!--card body-->
                    </div><!--card-->
                </div><!-- col-md-8 -->
            </div><!-- row -->
        </div><!-- container -->
    </div>
@endsection
